#
# Makefile
#

BUILD_DIR = ./build/

FLAGS_DEBUG = -DCMAKE_BUILD_TYPE=DEBUG --debug-output
FLAGS_TESTS = -DCMAKE_BUILD_TYPE=DEBUG --debug-output
FLAGS_RELEASE = -DCMAKE_BUILD_TYPE=RELEASE
CLANG_COMPILE = -DCMAKE_CXX_COMPILER=clang++

all: debug

debug: setup
	cd $(BUILD_DIR) && cmake .. $(FLAGS_DEBUG) $(CLANG_COMPILE) && cmake --build .

debug_gcc: setup
	cd $(BUILD_DIR) && cmake .. $(FLAGS_DEBUG) && cmake --build .

release: setup
	cd $(BUILD_DIR) && cmake .. $(FLAGS_RELEASE) $(CLANG_COMPILE) && cmake --build .

setup:
	if [ ! -d $(BUILD_DIR) ]; \
		then mkdir $(BUILD_DIR); \
	fi;

clean_client:
	if [ -d $(BUILD_DIR) ]; \
		then find build/CMakeFiles -name "*o" -delete; \
	fi;

clean:
	if [ -d $(BUILD_DIR) ]; \
		then rm -rf $(BUILD_DIR); \
	fi;

test: setup
	cd $(BUILD_DIR) && cmake .. $(FLAGS_TESTS) $(CLANG_COMPILE) && cmake --build . && make test

check: setup
	cd $(BUILD_DIR) && cmake .. $(FLAGS_TESTS) $(CLANG_COMPILE) && cmake --build . && make check

run: setup
	./build/bin/nochess

re: clean_client all
