#include "ReadDirectory.hpp"

#include <sys/types.h>
#include <dirent.h>
#include <regex>

std::vector<std::string> rd(const std::string &path, const std::string &str)
{
	DIR *dirp = opendir(path.c_str());
	struct dirent *dp;
	std::vector<std::string> data;
	const std::regex regex(str);

	while ((dp = readdir(dirp)) != NULL) {
		if (!std::regex_match(dp->d_name, regex))
			continue;
		data.push_back(dp->d_name);
	}
	closedir(dirp);
	return data;
}
