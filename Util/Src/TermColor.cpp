#include "TermColor.hpp"

std::ostream &operator<<(std::ostream &os, const TermColor &code) {
		return os << "\033[" << (int)code << "m";
}
