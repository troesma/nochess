#pragma once

#include <cstddef>

template <class T, size_t N>
class Vector;

using Pos = Vector<float, 2>;
using Size = Vector<float, 2>;

#include "Exception.hpp"

#include <array>
#include <ostream>
#include <iterator>
#include <algorithm>
#include <sstream>
#include <cstring>

template <class T, size_t N>
class Vector
{
	public:
	Vector();
	Vector(const std::array<T, N> &copy);
	virtual ~Vector();
	T &at(const size_t &);
	Vector<T, N> &operator=(const Vector<T, N> &copy);
	Vector<T, N> operator+(const Vector<T, N> &add);
	Vector<T, N> operator-(const Vector<T, N> &minus);
	Vector<T, N> operator*(const Vector<T, N> &mult);
	Vector<T, N> operator/(const Vector<T, N> &div);
	Vector<T, N> operator%(const Vector<T, N> &mod);
	bool operator==(const Vector<T, N> &cmp) const;
	bool operator<(const Vector<T, N> &cmp);
	bool operator>(const Vector<T, N> &cmp);
	bool operator!=(const Vector<T, N> &cmp);

	Vector<T, N> &operator=(const std::array<T, N> &copy);
	Vector<T, N> operator+(const std::array<T, N> &add);
	Vector<T, N> operator-(const std::array<T, N> &minus);
	Vector<T, N> operator*(const std::array<T, N> &mult);
	Vector<T, N> operator/(const std::array<T, N> &div);
	Vector<T, N> operator%(const std::array<T, N> &mod);
	bool operator==(const std::array<T, N> &cmp);
	bool operator!=(const std::array<T, N> &cmp);

	Vector<T, N> &operator=(const T &copy);
	Vector<T, N> operator+(const T &add);
	Vector<T, N> operator-(const T &minus);
	Vector<T, N> operator*(const T &mult);
	Vector<T, N> operator/(const T &div);
	Vector<T, N> operator%(const T &mod);

	T &operator[](size_t index);
	std::string str();

	public:
	std::array<T, N> array;
};

template <class T, size_t N>
std::ostream &operator<<(std::ostream &out, const Vector<T, N> &c);

template <class T, size_t N>
Vector<T, N>::Vector()
{
}

template <class T, size_t N>
T &Vector<T, N>::at(const size_t &i) {
	return (*this)[i];
}

template <class T, size_t N>
Vector<T, N>::Vector(const std::array<T, N> &copy)
    : array(copy)
{
}

template <class T, size_t N>
Vector<T, N>::~Vector()
{
}

// BEGINING const T &

template <class T, size_t N>
Vector<T, N> &Vector<T, N>::operator=(const T &copy)
{
	std::memset(array.begin(), copy, N * sizeof(T));
	return *this;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator+(const T &add)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] += add;
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator-(const T &minus)
{
	Vector<T, N> vec(*this);


	for (size_t i = 0; i < N; i++)
		vec.array[i] -= minus;
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator*(const T &mult)
{
	Vector<T, N> vec(*this);


	for (size_t i = 0; i < N; i++)
		vec.array[i] *= mult;
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator/(const T &div)
{
	Vector<T, N> vec(*this);


	for (size_t i = 0; i < N; i++)
		vec.array[i] /= div;
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator%(const T &mod)
{
	Vector<T, N> vec(*this);


	for (size_t i = 0; i < N; i++)
		vec.array[i] %= mod;
	return vec;
}

// BEGINING const Vector<T, N> &

template <class T, size_t N>
Vector<T, N> &Vector<T, N>::operator=(const Vector<T, N> &copy)
{
	std::memcpy(array.begin(), copy.array.begin(), N * sizeof(T));
	return *this;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator+(const Vector<T, N> &add)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] += add.array[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator-(const Vector<T, N> &minus)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] -= minus.array[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator*(const Vector<T, N> &mult)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] *= mult.array[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator/(const Vector<T, N> &div)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] /= div.array[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator%(const Vector<T, N> &mod)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] %= mod.array[i];
	return vec;
}

template <class T, size_t N>
bool Vector<T, N>::operator==(const Vector<T, N> &cmp) const
{
	for (size_t i = 0; i < N; i++)
		if (array[i] != cmp.array[i])
			return false;
	return true;
}

template <class T, size_t N>
bool Vector<T, N>::operator<(const Vector<T, N> &cmp)
{
	for (size_t i = 0; i < N; i++)
		if (array[i] < cmp.array[i])
			return false;
	return true;
}

template <class T, size_t N>
bool Vector<T, N>::operator>(const Vector<T, N> &cmp)
{
	for (size_t i = 0; i < N; i++)
		if (array[i] > cmp.array[i])
			return false;
	return true;
}

template <class T, size_t N>
bool Vector<T, N>::operator!=(const Vector<T, N> &cmp)
{
	return !(*this == cmp);
}

// BEGINNING std::array<T, N> &

template <class T, size_t N>
Vector<T, N> &Vector<T, N>::operator=(const std::array<T, N> &copy)
{
	std::memcpy(array.begin(), copy.begin(), N * sizeof(T));
	return *this;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator+(const std::array<T, N> &add)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] += add[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator-(const std::array<T, N> &minus)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] -= minus[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator*(const std::array<T, N> &mult)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] *= mult[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator/(const std::array<T, N> &div)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] /= div[i];
	return vec;
}

template <class T, size_t N>
Vector<T, N> Vector<T, N>::operator%(const std::array<T, N> &mod)
{
	Vector<T, N> vec(*this);

	for (size_t i = 0; i < N; i++)
		vec.array[i] %= mod[i];
	return vec;
}

template <class T, size_t N>
bool Vector<T, N>::operator==(const std::array<T, N> &cmp)
{
	for (size_t i = 0; i < N; i++)
		if (array[i] != cmp[i])
			return false;
	return true;
}

template <class T, size_t N>
bool Vector<T, N>::operator!=(const std::array<T, N> &cmp)
{
	return !(this == cmp);
}


template <class T, size_t N>
std::string Vector<T, N>::str()
{
	std::stringstream ss;
	ss << "{ ";
	for (auto it : array)
		ss << it << ' ';
	ss << "}";
	return ss.str();
}

template <class T, size_t N>
std::ostream &operator<<(std::ostream &out, const Vector<T, N> &c)
{
	out << "{ ";
	for (auto it : c.array)
		out << it << ' ';
	out << "}";
	return out;
}


template <class T, size_t N>
T &Vector<T, N>::operator[](size_t index)
{
	if (index > N) {
		std::stringstream ss;

		ss << "Asked for index " << index << " of vector of size " << N;
		throw err::excep(ss.str());
	}
	return array[index];
}
