#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <iostream>
#include <stdlib.h>
#include <dlfcn.h>

#include "Parser.hpp"
#include "Object.hpp"
#include "Array.hpp"

int main()
{
	Json::Parser *parser = new Json::Parser();

	try {
		auto par = parser->file("./Test/test.json");

		par->cast<Json::Object>()
			->child("object")
			->as<Json::Object>([](Json::Object &object) {
				object.child("2")
					->as<int>([](int &i) {
						std::cout << "Got: " << i << std::endl;
					})
					->parse();
			})
			->required()
			->child("object2")
			->as<Json::Object>([](Json::Object &object) {
				object.each<int>([](const std::string &str, int &i) {
						  std::cout << str << " -> " << i << std::endl;
					  })
					->parse();
			})
			->required()
			->child("array")
			->as<Json::Array>([](Json::Array &array) {
				array.at<std::string>(1, [](std::string &str) {
						 std::cout << "array string: " << str << std::endl;
					 })
					->required()
					->each<int>([](const uint32_t &index, int &i) {
						std::cout << "Got int: " << i << std::endl;
					})
					->parse();
			})
			->required()
			->child("deep")
			->as<Json::Object>([](Json::Object &object) {
				std::cout << "first" << std::endl;
				object.child("test")
					->as<Json::Object>([](Json::Object &object) {
						object.child("trying")
							->as<Json::Object>([](Json::Object &object) {
								object.child("very_hard")
									->as<Json::Object>([](Json::Object &object) {
										object.child("dont_exist")
											->as<bool>([](bool &b) {
												if (b == true) {
													std::cout << "Found bool: " << b << std::endl;
												} else {
													throw Json::Exception("test");
												}
											})
											->required()
											->parse();
									})
									->required()
									->parse();
							})
							->required()
							->parse();
					})
					->required()
					->parse();
			})
			->required()
			->parse();
	} catch (Json::Exception &e) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}
