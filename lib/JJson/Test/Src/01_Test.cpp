#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <iostream>
#include <stdlib.h>
#include <dlfcn.h>

#include <boost/filesystem.hpp>

#include "Parser.hpp"
#include "Object.hpp"
#include "Array.hpp"

#include "gtest/gtest.h"

namespace fs = boost::filesystem;

std::string data_path;

TEST(jjson_parser, opening_a_file_exist_object)
{
	Json::Parser *parser = new Json::Parser();

	try {
		auto par = parser->file(data_path + "test.json");

		par->cast<Json::Object>()
			->parse();
	} catch (Json::Exception &e) {
		FAIL() << e.what();
	}
}

TEST(jjson_parser, opening_a_file_doesnt_exist)
{
	Json::Parser *parser = new Json::Parser();

	try {
		auto par = parser->file(data_path + "notafile.json");

		par->cast<Json::Object>()
			->required()
			->parse();
		FAIL() << "The program should have catched an error";
	} catch (Json::Exception &e) {
	}
}

TEST(jjson_object, required_on_null_child)
{
	Json::Parser *parser = new Json::Parser();

	try {
		auto par = parser->file(data_path + "test.json");

		par->cast<Json::Object>()
			->required()
			->parse();
		FAIL() << "You shouldn't be able to put required on empty child";
	} catch (Json::Exception &e) {
	}
}

void setDataPath()
{
	fs::path data = fs::path(__FILE__).parent_path().parent_path();

	data += "/Data";
	if (data.empty()) {
		std::cerr << "No data repository" << std::endl;
		exit(1);
	}

	data_path = data.native() + '/';
}

void init()
{
	setDataPath();
}

int main(int argc, char **argv)
{
	init();
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
