#pragma once

namespace Json
{
	class Parser;
}

#include <string>

#include "Value.hpp"

namespace Json
{
	class Parser : public Value
	{
		public:
		Parser();
		Parser(Value *);
		~Parser() override;
		Json::Value *file(const std::string &path);

		private:
		rapidjson::Document *document;
	};
}
