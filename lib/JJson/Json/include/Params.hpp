#pragma once

namespace Json
{
	class Value;
	class Params;
	class Child;
}

#include "Value.hpp"

#include <functional>
#include <unordered_map>

namespace Json
{

	using GenericValue = rapidjson::GenericValue<rapidjson::UTF8<char>, rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator>>;

	class Params;
	class Array;
	class Object;

	extern Params *params;

	class Params
	{
		public:
		Params();
		virtual ~Params() = default;

		static void createIfNull()
		{
			if (params != nullptr)
				return;
			params = new Json::Params;
			if (params == nullptr)
				throw Json::Exception("Out of Memory");
		}

		template <class T>
		static std::function<bool(rapidjson::Value *)> executeHandler(Json::Child *child, std::function<void(T &)> &lb)
		{
			createIfNull();
			void *fp = params->lbManager[typeid(T).hash_code()];

			if (fp == nullptr)
				throw Json::Exception(std::string("No handler for type ") + typeid(T).name());
			return [child, lb, fp](rapidjson::Value *value) {
				return ((bool (*)(Json::Child *, std::function<void(T &)>, rapidjson::Value *))fp)(child, lb, value);
			};
		}

		//template <class T>
		//static void addHandler(std::function<void(T &)> fp)
		//{
		//createIfNull();
		//size_t hash = typeid(T).hash_code();

		//if (params->lbManager[hash])
		//std::cerr << "Overwriting previous result for " << typeid(T).name();
		//params->lbManager[hash] = fp;
		//}

		public:
		std::unordered_map<Json::HASH_CODE, void *> lbManager;
	};

}
