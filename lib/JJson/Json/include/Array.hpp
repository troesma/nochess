#pragma once

namespace Json
{
	class Array;
}

#include "Value.hpp"
#include "Child.hpp"

#include <memory>
#include <vector>

namespace Json
{

	class Array : public Value
	{
		public:
		Array(Value *value);
		Array(const Value &copy);

		virtual ~Array() = default;
		Json::Array *minSize(const uint32_t &size);
		size_t size();
		Array *parse();
		Array *required();

		template <class T>
		Array *at(uint32_t index, std::function<void(T &)> lbObject)
		{
			activeChild = std::make_shared<Child>(index, this);
			activeChild->setLb(lbObject);
			childs.push_back(activeChild);
			return this;
		}

		template <class T>
		Array *each(std::function<void(uint32_t &, T &)> lb)
		{
			activeChild = std::make_shared<Child>(this);
			activeChild->setLb(lb);
			eachChilds.push_back(activeChild);
			return this;
		}

		std::shared_ptr<Child> activeChild;
		std::vector<std::shared_ptr<Child>> eachChilds;
		std::vector<std::shared_ptr<Child>> childs;
	};

}
