#pragma once

namespace Json
{
	class Object;
	class Array;
}

#include "Value.hpp"
#include "Child.hpp"
#include "Parser.hpp"

#include <memory>
#include <vector>

namespace Json
{

	class Object : public Value
	{
		public:
		Object(Value *parent);
		// TODO: put copy in Array too
		Object(const Value &copy);
		virtual ~Object();
		Json::Object *minSize(const uint32_t &size);
		Object *child(const std::string &name);
		Object *parse();
		Object *required();

		template <class T>
		Object *as(std::function<void(T &)> lb)
		{
			activeChild = std::make_shared<Child>(childName, this);
			activeChild->setLb(lb);
			childs.push_back(activeChild);
			return this;
		}

		template <class T>
		Object *each(std::function<void(std::string &, T &)> lb)
		{
			activeChild = std::make_shared<Child>(this);
			activeChild->setLb(lb);
			eachChild.push_back(activeChild);
			return this;
		}

		std::string childName;
		std::shared_ptr<Child> activeChild;
		std::vector<std::shared_ptr<Child>> childs;
		std::vector<std::shared_ptr<Child>> eachChild;
	};
}
