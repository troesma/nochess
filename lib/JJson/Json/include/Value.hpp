#pragma once

namespace Json
{
	class Value;
	class Params;
}

#include "Exception.hpp"

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <functional>

#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <iostream>

namespace Json
{
	using HASH_CODE = size_t;

	enum Types {
		UNDEFINED,
		OBJECT,
		BOOL,
		STRING,
		ARRAY,
		INT_64,
		UINT,
	};

	class Value
	{
		public:
		Value(Json::Value *_parent = nullptr);
		Value(const Value &);
		virtual ~Value();
		//	Value *is(const Types &type);
		std::vector<Value *> getTree();
		void *getFp(Json::HASH_CODE);

		template <class T>
		std::shared_ptr<T> cast();

		std::string name;
		rapidjson::Value *value;
		Json::Value *parent;
		std::shared_ptr<Json::Params> params;
	};
}

template <class T>
std::shared_ptr<T> Json::Value::cast()
{
	return std::make_shared<T>((const Value &) *this);
}
