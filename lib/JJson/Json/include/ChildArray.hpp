#pragma once

namespace Json
{
	class ChildArray;
	class Object;
	class Array;
}

#include "Value.hpp"

#include <memory>
#include <functional>
#include <unordered_map>

namespace Json
{
	class ChildArray
	{
		public:
		ChildArray(Json::Value *_parent);

		template <class T>
		void setLb(Json::HASH_CODE, std::function<void(T)>);

		void setLb(std::function<void(Json::Object *)>);
		void setLb(std::function<void(Json::Array *)>);
		void setLb(std::function<void(int)>);
		void setLb(std::function<void(bool)>);
		void setLb(std::function<void(const std::string &)>);

		void setLb(std::function<void(const std::string &, Json::Object *)>);
		void setLb(std::function<void(const std::string &, Json::Array *)>);
		void setLb(std::function<void(const std::string &, int)>);
		void setLb(std::function<void(const std::string &, bool)>);
		void setLb(std::function<void(const std::string &, const std::string &)>);

		virtual ~ChildArray();

		std::function<void(rapidjson::Value *)> lb;
		bool required;
		uint64_t index;
		size_t size;
		Json::Value *parent;
	};

}
