#pragma once

namespace Json
{
	class Exception;
	class Value;
}

#include <exception>
#include <string>

namespace Json
{
	class Exception : public std::exception
	{
		public:
		Exception(const std::string &);
		Exception(Json::Value *value, const std::string &);
		virtual ~Exception();
		Exception &operator=(const Exception &);
		virtual const char *what() const noexcept override;

		std::string msg;
	};
}
