#pragma once

namespace Json
{
	class Child;
	class Object;
	class Array;
	class Parser;
}

#include "Value.hpp"
#include "Params.hpp"

#include <memory>
#include <functional>
#include <unordered_map>

namespace Json
{

	extern Params *params;

	class Child
	{
		public:
		Child(std::string &name, Json::Value *);
		Child(uint32_t &index, Json::Value *);
		Child(Json::Value *);
		virtual ~Child();

		template <class T>
		void setLb(std::function<void(T &)> &lbObject)
		{
			lb = Json::Params::executeHandler<T>(this, lbObject);
		}

		template <class T>
		void setLb(std::function<void(std::string &, T &)> &lbObject)
		{
			std::function<void(T &)> falseLb = [this, lbObject](T &value) {
				lbObject(this->name, value);
			};

			lb = Json::Params::executeHandler<T>(this, falseLb);
		}

		template <class T>
		void setLb(std::function<void(uint32_t &, T &)> &lbObject)
		{
			std::function<void(T &)> falseLb = [this, lbObject](T &value) {
				lbObject(this->index, value);
			};

			lb = Json::Params::executeHandler<T>(this, falseLb);
		}

		Json::Value *parent;
		std::function<bool(rapidjson::Value *)> lb;
		bool required;
		std::string name;
		uint32_t index;
	};

}
