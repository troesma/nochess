#include "Exception.hpp"
#include "Value.hpp"

namespace Json
{
	Exception::Exception(const std::string &str)
		: msg(str)
	{
	}

	Exception::Exception(Json::Value *value, const std::string &str)
		: msg(str)
	{
		std::vector<Value *> tree = value->getTree();

		for (Value *it : tree)
			msg.insert(0, it->name + " > ");
	}

	Exception::~Exception()
	{
	}

	const char *Exception::what() const noexcept
	{
		return msg.c_str();
	}

	Exception &Exception::operator=(const Exception &)
	{
		return *this;
	}
}
