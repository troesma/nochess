#include "Object.hpp"

namespace Json
{
	Object::Object(Value *value)
		: Value(value)
	{
		name = "UNDEFINED";
	}

	Object::Object(const Value &copy)
		: Value(copy)
	{
	}


	Object::~Object()
	{
	}

	Object *Object::child(const std::string &name)
	{
		childName = name;
		return this;
	}


	Object *Object::required()
	{
		if (activeChild == nullptr)
			throw Exception(this, "set required on null child");
		activeChild->required = true;
		return this;
	}

	Object *Object::parse()
	{
		if (value == nullptr)
			throw Exception(this, "Null Value");
		auto object = value->GetObject();

		for (auto it : childs) {
			// This may be moved below as it would get rid of a strcmp
			if (object.HasMember(it->name.c_str()) == false) {
				if (it->required == true)
					throw Exception(this, "Required Field doesn't exist");
				continue;
			}
			rapidjson::Value *tempValue = &object[it->name.c_str()];

			if (it->lb(tempValue) == false && it->required == true)
				throw Exception(this, "Required Field doesn't exist");
		}

		for (auto it : eachChild) {
			for (auto member = object.MemberBegin(); member != object.MemberEnd(); ++member) {
				it->name = member->name.GetString();
				rapidjson::Value *tempValue = &object[it->name.c_str()];

				it->lb(tempValue);
			}
		}


		return this;
	}

	Object *Object::minSize(const uint32_t &size)
	{
		if (value->Size() < size)
			throw Exception(this, "Invalid Size For Json Object.");
		return this;
	}
}
