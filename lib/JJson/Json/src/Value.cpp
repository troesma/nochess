#include "Value.hpp"

#include "Params.hpp"

namespace Json
{

	Value::Value(Value *_parent)
		: name("Value")
		, value(nullptr)
		, parent(_parent)
	{
		if (parent == nullptr || parent->params == nullptr)
			params = std::make_shared<Params>();
		else
			params = parent->params;
	}

	Value::Value(const Value &copy)
		: name(copy.name)
		, value(copy.value)
		, parent(copy.parent)
	{
		if (parent == nullptr || parent->params == nullptr)
			params = std::make_shared<Params>();
		else
			params = parent->params;
	}

	std::vector<Value *> Value::getTree()
	{
		std::vector<Value *> tree({ this });

		if (parent != nullptr) {
			auto pTree = parent->getTree();
			tree.insert(tree.end(), pTree.begin(), pTree.end());
		}
		return tree;
	}

	void *Value::getFp(Json::HASH_CODE code)
	{
		if (params == nullptr)
			throw Json::Exception(this, "Trying to access params when it is null.");
		auto fp = params->lbManager[code];

		if (fp == nullptr)
			throw Json::Exception(this, "function pointer is null can's access element with code : " + std::to_string(code));
		return fp;
	}

	Value::~Value()
	{
	}

	//Value *Value::is(size_t size)
	//{
	//if (value == nullptr)
	//throw Exception("Null Value");

	//switch (type) {
	//case OBJECT:
	//if (value->IsObject() == false)
	//throw Exception(this, "Wrong Type isn't object.");
	//break;
	//case BOOL:
	//if (value->IsBool() == false)
	//throw Exception(this, "Wrong Type isn't boolean.");
	//break;
	//case STRING:
	//if (value->IsString() == false)
	//throw Exception(this, "Wrong Type isn't string.");
	//break;
	//case ARRAY:
	//if (value->IsArray() == false)
	//throw Exception(this, "Wrong Type isn't array.");
	//break;
	//case INT_64:
	//if (value->IsInt64() == false)
	//throw Exception(this, "Wrong Type. isn't int64");
	//break;
	//case UINT:
	//if (value->IsUint() == false)
	//throw Exception(this, "Wrong Type isn't uint.");
	//break;
	//return this;
	//default:
	//throw Exception(this, "Default Error.");
	//break;
	//}
	//return this;
	//}

}
