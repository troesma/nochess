#include "Parser.hpp"

#include <cstdio>
#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>

namespace Json
{

	Parser::Parser()
	{
	}

	Parser::Parser(Value *value)
		: Value(*value)
	{
	}

	Parser::~Parser()
	{
	}

	Json::Value *Parser::file(const std::string &path)
	{
		FILE *fp = fopen(path.c_str(), "r");

		if (fp == nullptr)
			throw Json::Exception(this, "Coudln't open " + path);
		size_t lSize;
		char *buffer;
		size_t result;

		if (fp == nullptr)
			throw Json::Exception(this, "File Error");
		fseek(fp, 0, SEEK_END);
		lSize = ftell(fp);
		rewind(fp);
		buffer = (char *)malloc(sizeof(char) * (lSize + 1));
		if (buffer == nullptr)
			throw Json::Exception(this, "Memory Error");
		result = fread(buffer, 1, lSize, fp);
		if (result != lSize)
			throw Json::Exception(this, "Reading Error");
		buffer[result] = 0;
		fclose(fp);

		document = new rapidjson::Document;
		document->Parse(buffer);
		this->value = dynamic_cast<rapidjson::Value *>(document);
		if (this->value == nullptr)
			throw Json::Exception(this, "Couldn't make value out of document");
		free(buffer);
		name = path;
		return this;
	}
}
