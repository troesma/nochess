#include "Params.hpp"
#include "Child.hpp"
#include "Object.hpp"
#include "Array.hpp"
#include "Exception.hpp"

#include <vector>

namespace Json
{

	Json::Params *params;

	bool handlerArray(Json::Child *child, std::function<void(Json::Array &)> &lb, rapidjson::Value *v)
	{
		if (v->IsArray() == false)
			return false;
		auto array = Json::Array(child->parent);

		array.value = v;
		lb(array);
		return true;
	}

	bool handlerObject(Json::Child *child, std::function<void(Json::Object &)> &lb, rapidjson::Value *v)
	{
		if (v->IsObject() == false)
			return false;
		auto object = Json::Object(child->parent);

		object.value = v;
		object.name = child->name;
		lb(object);
		return true;
	}

	bool handlerString(Json::Child * /*child*/, std::function<void(std::string &)> &lb, rapidjson::Value *v)
	{
		if (v->IsString() == false)
			return false;
		std::string str = v->GetString();

		lb(str);
		return true;
	}

	bool handlerInt(Json::Child * /*child*/, std::function<void(int &)> &lb, rapidjson::Value *v)
	{
		if (v->IsInt64() == false)
			return false;
		int i = v->GetInt64();

		lb(i);
		return true;
	}

	bool handlerBool(Json::Child * /*child*/, std::function<void(bool &)> &lb, rapidjson::Value *v)
	{
		if (v->IsBool() == false)
			return false;
		bool b = v->GetBool();

		lb(b);
		return true;
	}

	bool handlerParser(Json::Child * /*child*/, std::function<void(Json::Parser &)> &lb, rapidjson::Value *v)
	{
		if (v->IsString() == false)
			return false;
		std::string str = v->GetString();

		Json::Parser parser;

		parser.file(str);
		lb(parser);
		return true;
	}

	Params::Params()
		: lbManager({
			{ typeid(Json::Object).hash_code(), (void *)&handlerObject },
			{ typeid(Json::Array).hash_code(), (void *)&handlerArray },
			{ typeid(int).hash_code(), (void *)&handlerInt },
			{ typeid(std::string).hash_code(), (void *)&handlerString },
			{ typeid(bool).hash_code(), (void *)&handlerBool },
			{ typeid(Json::Parser).hash_code(), (void *)&handlerParser },
		})
	{
	}

}
