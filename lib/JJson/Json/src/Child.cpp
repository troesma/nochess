#include "Child.hpp"

#include "Object.hpp"
#include "Array.hpp"
#include "Parser.hpp"

namespace Json
{

	Child::Child(std::string &name, Json::Value *_parent)
		: parent(_parent)
		, required(false)
		, name(name)
		, index((uint32_t)-1)
	{
	}

	Child::Child(uint32_t &index, Json::Value *_parent)
		: parent(_parent)
		, required(false)
		, index(index)
	{
	}

	Child::Child(Json::Value *_parent)
		: parent(_parent)
		, required(false)
	{
	}

	Child::~Child()
	{
	}

}
