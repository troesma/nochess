#include "Array.hpp"

namespace Json
{

	Array::Array(Value *value)
		: Value(*value)
	{
		name = "UNDEFINED";
	}

	Array::Array(const Value &copy)
		: Value(copy)
	{
	}

	Array *Array::minSize(const uint32_t &size)
	{
		if (value->Size() < size)
			throw Exception(this, "Invalid Size For Json Array.");
		return this;
	}

	size_t Array::size()
	{
		if (value == nullptr)
			throw Exception(this, "Size on null child.");
		return value->Size();
	}

	Array *Array::parse()
	{
		auto array = value->GetArray();
		uint32_t size = (uint32_t)array.Size();

		for (auto it : eachChilds) {
			for (uint32_t i = 0; i != size; i++) {
				it->index = i;
				it->lb(&array[it->index]);
			}
		}
		for (auto it : childs) {
			if (it->index > size)
				throw Exception(this, "Invalid Index given");
			rapidjson::Value *tempValue = &array[it->index];

			if (it->lb(tempValue) == false && it->required == true)
				throw Exception(this, "Required Field doesn't exist");
		}
		return this;
	}

	Array *Array::required()
	{
		if (activeChild == nullptr)
			throw Exception(this, "set required on null child");
		activeChild->required = true;
		return this;
	}
}
