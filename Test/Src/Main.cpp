#include <sys/types.h>
#include <dirent.h>
#include <regex>
#include <iostream>
#include <stdlib.h>
#include <dlfcn.h>

template <class T>
size_t getType(T classV)
{
	return typeid(classV).hash_code();
}

int main()
{
	std::cout << "Type: " << getType(5) << std::endl;
	std::cout << "Type: " << getType(5.f) << std::endl;
	std::cout << "Type: " << getType("string") << std::endl;
	return 0;
}
