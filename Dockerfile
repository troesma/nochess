FROM debian

RUN apt update && apt -y install make cmake clang python3-pip libgtest-dev libsdl2-dev libsdl2-image-dev libpng-dev libjpeg-dev

RUN pip3 install conan
