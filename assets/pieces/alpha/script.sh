#!/bin/bash

for i in $(ls | grep ".svg$"); do
	R="$(echo $i | cut -d'.' -f 1)"
	inkscape -z -e $R.png -w 256 -h 256 $R.svg
done;
