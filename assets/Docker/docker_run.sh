#!/bin/bash

NAME=epitechcontent/epitest-docker
COMMAND=zsh

sudo docker run -v $PWD/../:/workspace -it --rm $NAME $COMMAND
