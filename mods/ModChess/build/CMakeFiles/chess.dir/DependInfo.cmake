# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/emilien/2019/nochess/mods/ModChess/src/Atomic.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/Atomic.cpp.o"
  "/home/emilien/2019/nochess/mods/ModChess/src/Castle.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/Castle.cpp.o"
  "/home/emilien/2019/nochess/mods/ModChess/src/Chess.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/Chess.cpp.o"
  "/home/emilien/2019/nochess/mods/ModChess/src/Entry.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/Entry.cpp.o"
  "/home/emilien/2019/nochess/mods/ModChess/src/Evolution.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/Evolution.cpp.o"
  "/home/emilien/2019/nochess/mods/ModChess/src/GameDeath.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/GameDeath.cpp.o"
  "/home/emilien/2019/nochess/mods/ModChess/src/PawnOrigin.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/PawnOrigin.cpp.o"
  "/home/emilien/2019/nochess/mods/ModChess/src/Rock.cpp" "/home/emilien/2019/nochess/mods/ModChess/build/CMakeFiles/chess.dir/src/Rock.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "chess_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../../data/include"
  "../../data/include-oxygine"
  "../../data/include-util"
  "../../data/include-json"
  "/usr/include/SDL2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
