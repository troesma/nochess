#include "Atomic.hpp"

#include <iostream>

Atomic::Atomic(Piece *piece, Json::Value *)
	: Specials(piece)
{
}

Atomic::~Atomic()
{
}

Specials *AtomicCreate(Piece *piece, Json::Value *value)
{
	Specials *ptr = new Atomic(piece, value);

	if (ptr == nullptr)
		throw err::excep("Out Of Memory.");
	return ptr;
}

void Atomic::afterMove()
{
	auto it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecLeft);

	if (it == piece->allMoves.end())
		return;
	piece->allMoves.erase(it);
	it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecRight);

	if (it == piece->allMoves.end())
		return;
	piece->allMoves.erase(it);
	destroy();
}

void Atomic::onInit()
{
	vecLeft = std::vector<Vector<int16_t, 2>>({
		Vector<int16_t, 2>({ 3, 0 }),
	});
	vecRight = std::vector<Vector<int16_t, 2>>({
		Vector<int16_t, 2>({ -2, 0 }),
	});
	piece->allMoves.push_back(vecLeft);
	piece->allMoves.push_back(vecRight);
	std::cout << "Piece Event On Init" << std::endl;
}
