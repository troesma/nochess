#include "Evolution.hpp"

#include <iostream>

Evolution::Evolution(Piece *piece, Json::Value *)
	: Specials(piece)
{
}

Evolution::~Evolution()
{
}

Specials *EvolutionCreate(Piece *piece, Json::Value *value)
{
	Specials *ptr = new Evolution(piece, value);

	if (ptr == nullptr)
		throw err::excep("Out Of Memory.");
	return ptr;
}

void Evolution::afterMove()
{
	if (piece->square->xy[1] != (piece->loader->xy[1] - 1) && piece->square->xy[1] != 0)
		return;
	auto ptr = piece->loader->config->piecesCfg["queen"];

	if (ptr == nullptr)
		throw err::excep("Piece 'queen' doesn't exist.");
	if (piece->config->owner == WHITE) {
		//piece->loader->getGraphical()->loadImage(ptr->white, ptr->white);
		//piece->piece->hide(true);
		//piece->piece = piece->square->group->addSprite(ptr->white, LEVEL_PIECE);
	}
	if (piece->config->owner == BLACK) {
		//piece->loader->getGraphical()->loadImage(ptr->black, ptr->black);
		//piece->piece->hide(true);
		//piece->piece = piece->square->group->addSprite(ptr->black, LEVEL_PIECE);
	}
	piece->generateConfig(ptr);
}
