#include "Chess.hpp"
#include "Object.hpp"
#include "Array.hpp"

#include <iostream>

Chess::Chess(Piece *piece, Json::Value *value)
	: Specials(piece)
{
	try {
		value->is(Json::OBJECT)
			->cast<Json::Object>()
			->child("conditions")
			->as([this](Json::Object *object) {
				object->child("positions")
					->as([this](Json::Array *array) {
						array->forEach([this](Json::Array *array) {
								 std::string pieceName;
								 Vector<int, 5> pos;

								 if (array->size() != 3)
									 throw Json::Exception(array, "Invalid array size.");
								 int incr = 0;

								 array->at(0, [&pieceName](const std::string &_name) {
										  pieceName = _name;
									  })
									 ->forEach(std::function<void(int)>([&pos, &incr](int i) {
										 pos[incr] = i;
										 incr++;
									 }))
									 ->parse();
								 this->positions.push_back(std::pair<std::string, Vector<int, 5>>(pieceName, pos));
							 })
							->parse();
					})
					->child("notmenaced")
					->as([this](Json::Array *array) {
						array->forEach([this](Json::Array *array) {
								 Vector<int, 2> tmp;

								 if (array->size() != 2)
									 throw Json::Exception(array, "Invalid array size.");
								 int incr = 0;

								 array->forEach(std::function<void(int)>([&tmp, &incr](int i) {
										  tmp[incr] = i;
										  incr++;
									  }))
									 ->parse();
								 this->notmenaced.push_back(tmp);
							 })
							->parse();
					})
					->child("nevermoved")
					->as([this](Json::Array *array) {
						array->forEach([this](Json::Array *array) {
								 std::string pieceName;
								 Vector<int, 5> pos;

								 if (array->size() != 3)
									 throw Json::Exception(array, "Invalid array size.");
								 int incr = 0;

								 array->at(0, [&pieceName](const std::string &_name) {
										  pieceName = _name;
									  })
									 ->forEach(std::function<void(int)>([&pos, &incr](int i) {
										 pos[incr] = i;
										 incr++;
									 }))
									 ->parse();
								 this->nevermoved.push_back(std::pair<std::string, Vector<int, 5>>(pieceName, pos));
							 })
							->parse();
					})
					->parse();
			})
			->child("actions")
			->as([this](Json::Object *object) {
				object->child("move")
					->as([this](Json::Array *array) {
						array->at(0, [](Json::Array *array) {
								 if (array->size() != 2)
									 throw Json::Exception(array, "Invalid array size.");
							 })
							->required()
							->at(1, [](Json::Array *array) {
								if (array->size() != 5)
									throw Json::Exception(array, "Invalid array size.");
							})
							->required()
							->parse();
					})
					->required()
					->parse();
			})
			->parse();
	} catch (const Json::Exception &c) {
		throw err::excep(c.what());
	}
}

Chess::~Chess()
{
}

void Chess::atTurn() {}

void Chess::afterTurn() {}

void Chess::atMove() {}

void Chess::afterMove() {}

void Chess::onEaten() {}

void Chess::onInit() {}

extern "C" {

Specials *ChessCreate(Piece *piece, Json::Value *value)
{
	Specials *ptr = dynamic_cast<Specials *>(new Chess(piece, value));

	if (ptr == nullptr)
		throw err::excep("Out Of Memory.");
	return ptr;
}
}

//void Chess::afterMove()
//{
////auto it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecLeft);

////if (it == piece->allMoves.end())
////return;
////piece->allMoves.erase(it);
////it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecRight);

////if (it == piece->allMoves.end())
////return;
////piece->allMoves.erase(it);
////destroy();
//}

//void Chess::onInit()
//{
////vecLeft = std::vector<Vector<int16_t, 2>>({
////Vector<int16_t, 2>(3, 0),
////});
////vecRight = std::vector<Vector<int16_t, 2>>({
////Vector<int16_t, 2>(-2, 0),
////});
////std::cout << piece->allMoves.size() << std::endl;
////piece->allMoves.push_back(vecLeft);
////piece->allMoves.push_back(vecRight);
////std::cout << "Piece Event On Init" << std::endl;
//}
