#include "Entry.hpp"

#include <stdio.h>
#include <map>

extern "C" {

ModData *entry()
{
	ModData *data = new ModData;

	data->data = new std::map<const std::string, Specials::Initializer>(
		{
			{ "chess", &ChessCreate },
			{ "evolution", &EvolutionCreate },
			{ "game_death", &GameDeathCreate },
			{ "pawn_origin", &PawnOriginCreate },
			{ "rock", &RockCreate },
		});

	return data;
}
}
