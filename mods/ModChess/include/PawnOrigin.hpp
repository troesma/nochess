#pragma once

class PawnOrigin;

#include "NoChess/Specials.hpp"
#include "NoChess/Piece.hpp"
#include "Vector.hpp"

#include <vector>

class PawnOrigin : public virtual Specials
{
	public:
	PawnOrigin(Piece *piece, Json::Value *);
	virtual ~PawnOrigin();

	void afterMove();
	void onInit();

	private:
	std::vector<Vector<int16_t, 2>> vec;
};

extern "C" {
Specials *PawnOriginCreate(Piece *piece, Json::Value *value);
}
