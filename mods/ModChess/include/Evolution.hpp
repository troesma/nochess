#pragma once

class Evolution;

#include "NoChess/Specials.hpp"
#include "NoChess/Piece.hpp"

class Evolution : public virtual Specials
{
	public:
	Evolution(Piece *, Json::Value *);
	virtual ~Evolution();

	void afterMove();
};

extern "C" {
Specials *EvolutionCreate(Piece *piece, Json::Value *value);
}
