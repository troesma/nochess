#pragma once

class Piece;
class Specials;

namespace Json {
	class Value;
}

#include "NoChess/SpecialsLoaderPiece.hpp"

#include "Chess.hpp"
#include "Evolution.hpp"
#include "GameDeath.hpp"
#include "PawnOrigin.hpp"
#include "Rock.hpp"
