#pragma once

class Castle;

#include "NoChess/Specials.hpp"
#include "NoChess/Piece.hpp"
#include "Vector.hpp"

#include <vector>

class Castle : public virtual Specials
{
	public:
	Castle(Piece *, Json::Value *);
	virtual ~Castle();
	static Specials *create(Piece *piece, Json::Value *);

	void afterMove();
	void onInit();

	private:
	std::vector<Vector<int, 2>> notmenaced;
	std::vector<std::pair<std::string, Vector<int, 5>>> positions;
	std::vector<std::pair<std::string, Vector<int, 5>>> nevermoved;
	std::vector<std::pair<Vector<int, 2>, Vector<int, 5>>> moves;
};

extern "C" {
Specials *CastleCreate(Piece *piece, Json::Value *value);
}
