#pragma once

class Chess;

#include "NoChess/Piece.hpp"
#include "Value.hpp"
#include "NoChess/Specials.hpp"
#include "Vector.hpp"

#include <vector>

class Chess : public virtual Specials
{
	public:
	Chess(Piece *, Json::Value *);
	virtual ~Chess();
	void atTurn();
	void afterTurn();
	void atMove();
	void afterMove();
	void onEaten();
	void onInit();

	private:
	std::vector<Vector<int, 2>> notmenaced;
	std::vector<std::pair<std::string, Vector<int, 5>>> positions;
	std::vector<std::pair<std::string, Vector<int, 5>>> nevermoved;
	std::vector<std::pair<Vector<int, 2>, Vector<int, 5>>> moves;
};

extern "C" {
Specials *ChessCreate(Piece *piece, Json::Value *value);
}
