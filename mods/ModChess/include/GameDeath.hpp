#pragma once

class GameDeath;

#include "NoChess/Specials.hpp"
#include "NoChess/Piece.hpp"
#include "Vector.hpp"

#include <vector>

class GameDeath : public virtual Specials
{
	public:
	GameDeath(Piece *piece, Json::Value *);
	virtual ~GameDeath();

	void onEaten();

	private:
	std::vector<Vector<int16_t, 2>> vec;
};

extern "C" {
Specials *GameDeathCreate(Piece *piece, Json::Value *value);
}
