#pragma once

class ChessSpecials;

class ChessSpecials
{
	public:
	ChessSpecials();
	virtual ~ChessSpecials();

	virtual void atTurn() {};
	virtual void afterTurn() {};
	virtual void atMove() {};
	virtual void afterMove() {};
	virtual void onEaten() {};
	virtual void onInit() {};
};
