#pragma once

class Position;

#include "Chess/ChessSpecials.hpp"

class Position : public ChessSpecials
{
	public:
	Position();
	virtual ~Position();
};
