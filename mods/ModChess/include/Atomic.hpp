#pragma once

class Atomic;

#include "NoChess/Specials.hpp"
#include "NoChess/Piece.hpp"
#include "Vector.hpp"

#include <vector>

class Atomic : public virtual Specials
{
	public:
	Atomic(Piece *, Json::Value *);
	virtual ~Atomic();

	void afterMove();
	void onInit();

	private:
	std::vector<Vector<int16_t, 2>> vecLeft;
	std::vector<Vector<int16_t, 2>> vecRight;
};

extern "C" {
Specials *AtomicCreate(Piece *piece, Json::Value *value);
}
