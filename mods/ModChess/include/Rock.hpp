#pragma once

class Rock;

#include "NoChess/Specials.hpp"
#include "NoChess/Piece.hpp"
#include "Vector.hpp"

#include <vector>

class Rock : public virtual Specials
{
	public:
	Rock(Piece *, Json::Value *);
	virtual ~Rock();

	void afterMove();
	void onInit();

	private:
	std::vector<Vector<int16_t, 2>> vecLeft;
	std::vector<Vector<int16_t, 2>> vecRight;
};

extern "C" {
Specials *RockCreate(Piece *piece, Json::Value *value);
}
