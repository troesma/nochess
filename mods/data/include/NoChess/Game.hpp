#pragma once

class Game;

#include "GameLoader.hpp"
#include "Menu.hpp"
#include "Iparameter.hpp"
#include "Graphical.hpp"

#include <memory>

class Game {
	public:
	Game(IParameter *);
	~Game();
	void init();
	void start();
	void update();
	void launchGame(const std::string &path);
	public:
	int turn;

	public:
	std::shared_ptr<Graphical> g;
	std::shared_ptr<GameLoader> loader;

	private:
	IParameter *parameter;
	std::shared_ptr<Menu> menu;
};
