#pragma once

class PawnOrigin;

#include "Specials.hpp"
#include "Vector.hpp"

#include <vector>

class PawnOrigin : public Specials
{
	public:
	PawnOrigin(Piece *piece, Json::Value *);
	virtual ~PawnOrigin();
	static Specials *create(Piece *piece, Json::Value *);

	void afterMove();
	void onInit();

	private:
	std::vector<Vector<int16_t, 2>> vec;
};
