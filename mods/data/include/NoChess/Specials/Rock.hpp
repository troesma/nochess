#pragma once

class Rock;

#include "Specials.hpp"
#include "Vector.hpp"

#include <vector>

class Rock : virtual Specials
{
	public:
	Rock(Piece *, Json::Value *);
	virtual ~Rock();
	static Specials *create(Piece *piece, Json::Value *);

	void afterMove();
	void onInit();

	private:
	std::vector<Vector<int16_t, 2>> vecLeft;
	std::vector<Vector<int16_t, 2>> vecRight;
};
