#pragma once

class Castle;

#include "Specials.hpp"
#include "Vector.hpp"

#include <vector>

class Castle : virtual Specials
{
	public:
	Castle(Piece *, Json::Value *);
	virtual ~Castle();
	static Specials *create(Piece *piece, Json::Value *);

	void afterMove();
	void onInit();

	private:
	std::vector<Vector<int, 2>> notmenaced;
	std::vector<std::pair<std::string, Vector<int, 5>>> positions;
	std::vector<std::pair<std::string, Vector<int, 5>>> nevermoved;
	std::vector<std::pair<Vector<int, 2>, Vector<int, 5>>> moves;
};
