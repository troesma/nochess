#pragma once

class GameDeath;

#include "Specials.hpp"
#include "Vector.hpp"

#include <vector>

class GameDeath : public Specials
{
	public:
	GameDeath(Piece *piece, Json::Value *);
	virtual ~GameDeath();
	static Specials *create(Piece *piece, Json::Value *);

	void onEaten();

	private:
	std::vector<Vector<int16_t, 2>> vec;
};
