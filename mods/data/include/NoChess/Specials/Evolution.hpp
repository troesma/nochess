#pragma once

class Evolution;

#include "Specials.hpp"

class Evolution : virtual Specials
{
	public:
	Evolution(Piece *, Json::Value *);
	virtual ~Evolution();
	static Specials *create(Piece *piece, Json::Value *);

	void afterMove();
};
