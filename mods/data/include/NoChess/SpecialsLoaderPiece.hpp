#pragma once

class SpecialsLoaderPiece;
class Piece;

#include "Specials.hpp"
#include "Object.hpp"
#include "GameLoader.hpp"

#include <vector>
#include <map>
#include <rapidjson/document.h>

struct ModData {
	std::map<const std::string, Specials::Initializer> *data;
};

class SpecialsLoaderPiece
{
	public:
	SpecialsLoaderPiece(GameLoader *loader, Piece *piece);
	virtual ~SpecialsLoaderPiece();
	void instanciateSpecial(const std::string &, Json::Value *value);
	std::vector<Specials *> specials;

	private:
	GameLoader *loader;
	Piece *piece;
};
