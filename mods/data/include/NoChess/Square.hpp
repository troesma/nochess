#pragma once

class Square;

#include "GameLoader.hpp"
#include "Graphical.hpp"
#include "Piece.hpp"
#include "SpecialsLoaderSquare.hpp"

#include <stdint.h>

enum SquareEvent {
	SQUARE_EVENT_NONE,
	SQUARE_EVENT_ON_CALCULATE_MOVE,
	SQUARE_EVENT_AFTER_TURN,
	SQUARE_EVENT_AT_MOVE,
	SQUARE_EVENT_AFTER_MOVE,
	SQUARE_EVENT_ON_INIT,
	SQUARE_EVENT_ON_EATEN,
};
enum SquareLevel : uint8_t {
	LEVEL_NONE,
	LEVEL_SQUARE,
	LEVEL_HIGHLIGHT,
	LEVEL_PIECE,
	LEVEL_EFFECT
};

class Square
{
	public:
	Square(GameLoader *loader, Vector<int16_t, 2> xy, const SquaresConfig *config);
	virtual ~Square();
	void addPiece(Piece *piece);
	void init();
	void buttonEvent(ox::Event *);
	void slideEventDown(ox::Event *);
	void buttonClick();
	void highlight(bool, bool);
	void playTurn();

	void specialCalls(const SquareEvent &event);
	void loadSpecial();

	public:
	Vector<int16_t, 2> xy;
	Piece *piece;
	ox::spActor group;
	ox::spSprite tile;
	ox::ColorRectSprite *hiAlly;
	ox::ColorRectSprite *hiEnemy;

	private:
	const SquaresConfig *config;
	std::shared_ptr<SpecialsLoaderSquare> loaderList;
	GameLoader *loader;
};
