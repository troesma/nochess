#pragma once

class Menu;

#include "Graphical.hpp"
#include "MenuItem.hpp"

#include <vector>
#include <string>

class Menu
{
	public:
	Menu(Game *game);
	virtual ~Menu();
	void start(std::shared_ptr<Graphical>);
	void readDirectory(const std::string &name);
	void backgroundImage();
	void dialogBox();
	void titleSprite();
	void setBoxContent();
	void startGame(const std::string &);

	public:
	ox::ClipRectActor *box;
	Game *game;
	ox::spActor group;
	ox::spSprite bg;
	ox::spSprite title;
	ox::spBox9Sprite dialog;

	private:
	std::vector<std::string> gameList;
	std::vector<MenuItem *> items;
	std::shared_ptr<Graphical> g;
};
