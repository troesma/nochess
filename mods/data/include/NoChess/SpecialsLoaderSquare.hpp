#pragma once

class SpecialsLoaderSquare;
class Square;

#include "Specials.hpp"

#include <vector>
#include <map>
#include <rapidjson/document.h>

class SpecialsLoaderSquare
{
	public:
	SpecialsLoaderSquare(Square *piece);
	std::map<const std::string, const Specials::Initializer> loader();
	virtual ~SpecialsLoaderSquare();
	void instanciateSpecial(const std::string &, rapidjson::Value::ConstMemberIterator &value);
	std::vector<Specials*> specials;

	private:
	Square *square;
	Square *piece;
	std::map<const std::string, const Specials::Initializer> specialsInitializer;
};
