#!/bin/bash
#
# make_mods.sh
#

for i in `ls | grep 'Mod*'`; do
	cd "$i"
	make && make install
	cd ..
done;
