#include "Graphical.hpp"

Graphical::Graphical()
{
}

Graphical::~Graphical()
{
}

void Graphical::init()
{
	ox::ObjectBase::__startTracingLeaks();
	desc.title = "HUB_Chess";

	desc.w = 1600;
	desc.h = 900;

	ox::core::init(&desc);
	ox::Stage::instance = new ox::Stage();
	ox::Point size = ox::core::getDisplaySize();
	ox::getStage()->setSize(size);
	//ox::DebugActor::show();
	images = std::make_shared<Images>();
	fonts = std::make_shared<Fonts>();
}

bool Graphical::update()
{
	bool state;

	state = ox::core::update();
	ox::getStage()->update();
	if (ox::core::beginRendering()) {
		ox::Color clearColor(11, 11, 11, 255);
		ox::Rect viewport(ox::Point(0, 0), ox::core::getDisplaySize());
		ox::getStage()->render(clearColor, viewport);
		ox::core::swapDisplayBuffers();
	}
	return !state;
}

void Graphical::predestroy()
{
	ox::ObjectBase::dumpCreatedObjects();
}

void Graphical::destroy()
{
	ox::core::release();
	ox::ObjectBase::dumpCreatedObjects();
	ox::ObjectBase::__stopTracingLeaks();
}
