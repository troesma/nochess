#include "GameLoader.hpp"
#include "ReadDirectory.hpp"

#include <string>
#include <stdlib.h>
#include <dlfcn.h>

GameLoader::GameLoader(Game *game, std::string configFile)
	: game(game)
	, config(new GameConfig(this, configFile))
	, gameEnded(false)
	, turn(0)
{
	libLoader();
	config->checkIntegrity();
	SquaresSpecialsConfig *base = config->squaresSpecialsCfg[config->mapCfg->base].get();

	if (base == nullptr)
		throw err::excep("Invalid base tile not set in field squares");
	xy = config->mapCfg->xy;
}

void GameLoader::win(const Team &winner)
{
	std::cout << "Congrats team " << (int)winner << std::endl;
	gameEnded = true;
}

void GameLoader::start(std::shared_ptr<Graphical> g)
{
	this->g = g;
	board = new Board(this);
	board->init();
	for (auto it : config->teamsCfg) {
		Vector<int, 2> pos({ it.first[0], (int)(xy[1] - it.first[1] + 1) });
		Square *square = board->at(pos);
		Piece *piece = new Piece(this, square, it.second.get());

		pieces.push_back(piece);
		piece->start();
	}
}

GameLoader::~GameLoader()
{
	board->zone->setVisible(false);
	delete config;
	delete board;
	for (auto it : pieces) {
		delete it;
	}
}

std::map<const std::string, Specials::Initializer> &GameLoader::libLoader()
{
	auto data = rd("./mods", "lib.+\\.so");

	for (auto it : data) {
		void *handle;
		ModData *(*entry)();
		char *error;

		std::cout << "Opening: " << it << std::endl;
		handle = dlopen(("./mods/" + it).c_str(), RTLD_NOW);
		if ((error = dlerror()) != NULL) {
			std::cerr << "Skipping Mod: " << it << ": " << error << std::endl;
			continue;
		}

		*(void **)(&entry) = dlsym(handle, "entry");

		if ((error = dlerror()) != NULL) {
			std::cerr << "Skipping Mod: " << it << ": " << error << std::endl;
			continue;
		}

		auto rdata = *(*entry)()->data;
		for (auto field : rdata) {
			if (specialsInitializer[field.first] != nullptr)
				throw err::excep("Collision on special " + field.first);
			specialsInitializer[field.first] = field.second;
		}
		handles.push_back(handle);
	}
	return specialsInitializer;
}
