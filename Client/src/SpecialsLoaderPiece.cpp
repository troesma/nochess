#include "SpecialsLoaderPiece.hpp"
#include "ReadDirectory.hpp"

#include <algorithm>
#include <dlfcn.h>

SpecialsLoaderPiece::SpecialsLoaderPiece(GameLoader *loader, Piece *piece)
	: loader(loader)
	, piece(piece)
{
}

void SpecialsLoaderPiece::instanciateSpecial(const std::string &name, Json::Value *value)
{
	size_t size = name.find_first_of("#\0");
	std::string tmpName;

	if (!name[size]) {
		tmpName = name;
	} else {
		tmpName = std::string(name.begin(), name.begin() + size);
	}
	auto data = loader->specialsInitializer[tmpName];

	if (data == nullptr)
		throw err::excep("Couldn't initialize '" + name + "' no rules set.");
	Specials *special = (*data)(piece, value);

	special->name = name;
	specials.push_back(special);
}

SpecialsLoaderPiece::~SpecialsLoaderPiece()
{
	for (auto it : specials)
		delete it;
	//dlclose(handle);
}
