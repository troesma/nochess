#include "Piece.hpp"

Piece::Piece(GameLoader *loader, Square *square, const TeamConfig *config)
	: square(square)
	, loader(loader)
	, config(config)
	, moved(false)
{
	setupPiece(config->piece);
}

Piece::~Piece()
{
	texture->removeAllEventListeners();
	texture->setVisible(false);
}

void Piece::move(ox::Event *event)
{
	ox::TouchEvent *te = ox::safeCast<ox::TouchEvent *>(event);

	const ox::Vector2 &pos = te->localPosition;

	ox::RectT<ox::Vector2> intersect(pos.x, pos.y, 1, 1);
	ox::RectT<ox::Vector2> area(square->group->getPosition(), square->group->getSize());
	texture->setPosition(pos - (texture->getScaledSize() / 2));

	if (area.isIntersecting(intersect) == true)
		return;
	if (moved == false)
		moved = true;
}

void Piece::setupPiece(std::shared_ptr<PieceConfig> config)
{
	if (allMoves.empty() == false) {
		allMoves.clear();
		allAttacks.clear();
	}
	generateConfig(config);
	loadSpecial();
}

void Piece::generateConfig(std::shared_ptr<PieceConfig> config)
{
	int tmpint[2];
	int i = 0;
	std::vector<Vector<int16_t, 2>> *tmp;
	for (auto it : config->movement) {
		i = 0;
		tmp = new std::vector<Vector<int16_t, 2>>;
		while (i <= it[4]) {
			tmpint[0] = (it[0] * i + it[2]);
			tmpint[1] = (it[1] * i + it[3]);
			if (tmpint[0] != 0 || tmpint[1] != 0) {
				tmp->push_back(Vector<int16_t, 2>({(int16_t)tmpint[0], (int16_t)tmpint[1]}));
			}
			i++;
		}
		allMoves.push_back(*tmp);
	}
	for (auto it : config->eating) {
		i = 0;
		tmp = new std::vector<Vector<int16_t, 2>>;
		while (i <= it[4]) {
			tmpint[0] = (it[0] * i + it[2]);
			tmpint[1] = (it[1] * i + it[3]);
			if (tmpint[0] != 0 || tmpint[1] != 0) {
				tmp->push_back(Vector<int16_t, 2>({(int16_t)tmpint[0], (int16_t)tmpint[1]}));
			}
			i++;
		}
		allAttacks.push_back(*tmp);
	}
}

void Piece::select()
{
	if (moves.empty() == true)
		moves = listMoves();
	for (auto it : moves) {
		loader->board->at(it[0] + 1, it[1] + 1)->highlight(true, (loader->turn % 2) == (config->owner - 1));
	}
}

void Piece::unselect()
{
	for (auto it : moves) {
		loader->board->at(it[0] + 1, it[1] + 1)->highlight(false, true);
	}
	moves.clear();
}

void Piece::getEatenBy(Piece *eater)
{
	specialCalls(PIECE_EVENT_ON_EATEN);
	this->square = nullptr;
	texture->detach();
	(void)eater;
}

void Piece::resetScale(ox::Vector2 tsize)
{
	Vector<float, 2> size({tsize.x, tsize.y});
	Vector<float, 2> scale = size / Vector<float, 2>({image->getWidth(), image->getHeight()});

	texture->setScale(scale[0], scale[1]);
}

void Piece::start()
{
	Vector<float, 4> area = loader->board->area;
	if (config->owner == WHITE)
		image = loader->g->images->create(config->piece->white);
	if (config->owner == BLACK)
		image = loader->g->images->create(config->piece->black);
	texture = new ox::Sprite();
	resetScale(loader->board->scale);
	texture->setPosition(0, 0);
	texture->setResAnim(image);
	texture->attachTo(square->group);
	square->piece = this;
	specialCalls(PIECE_EVENT_ON_INIT);
}

bool Piece::moveTo(Square *newsquare)
{
	this->unselect();
	if ((loader->turn % 2) == (config->owner - 1) || !possibleMove(newsquare)) {
		texture->detach();
		texture->attachTo(square->group);
		return false;
	}
	square->piece = NULL;
	square = newsquare;
	specialCalls(PIECE_EVENT_AT_MOVE);
	square->addPiece(this);
	moves.clear();
	specialCalls(PIECE_EVENT_AFTER_MOVE);
	return true;
}

std::vector<Vector<int16_t, 2>> Piece::listMoves()
{
	std::vector<Vector<int16_t, 2>> tmp;

	for (auto it : allMoves) {
		int stop = 0;
		for (auto it2 : it) {
			if ((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) < loader->xy[0] && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) < loader->xy[1]
				&& (square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) >= 0 && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) >= 0) {
				if (loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece != nullptr)
					stop = 1;
				if (stop == 0)
					tmp.push_back(Vector<int16_t, 2>({(int16_t)(square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)), (int16_t)(square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1))}));
			}
		}
	}
	for (auto it : allAttacks) {
		int stop = 0;
		for (auto it2 : it) {
			if ((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) < loader->xy[0] && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) < loader->xy[1] && (square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) >= 0 && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) >= 0) {
				if (stop == 0
					&& loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece != nullptr
					&& loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece->config->owner != config->owner)
					tmp.push_back(Vector<int16_t, 2>({(int16_t)(square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)), (int16_t)(square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1))}));
				if (loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece != nullptr)
					stop = 1;
			}
		}
	}
	return tmp;
}

bool Piece::possibleMove(Square *newsquare)
{
	for (auto it : allMoves) {
		int stop = 0;
		for (auto it2 : it) {
			if ((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) < loader->xy[0] && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) < loader->xy[1] && (square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) >= 0 && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) >= 0) {
				if (loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece != nullptr)
					stop = 1;
				if ((square->xy + it2 * (config->owner == WHITE ? -1 : 1)) == newsquare->xy && stop == 0)
					return true;
			}
		}
	}
	for (auto it : allAttacks) {
		int stop = 0;
		for (auto it2 : it) {
			if ((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) < loader->xy[0] && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) < loader->xy[1] && (square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1)) >= 0 && (square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)) >= 0) {
				if ((square->xy + it2 * (config->owner == WHITE ? -1 : 1)) == newsquare->xy
					&& stop == 0 && loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece != nullptr
					&& loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece->config->owner != config->owner)
					return true;
				if (loader->board->board.at((square->xy[0] + it2[0] * (config->owner == WHITE ? -1 : 1))).at((square->xy[1] + it2[1] * (config->owner == WHITE ? -1 : 1)))->piece != nullptr)
					stop = 1;
			}
		}
	}
	return false;
}

void Piece::loadSpecial()
{
	loaderList = std::make_shared<SpecialsLoaderPiece>(loader, this);
	for (auto it : config->piece->params) {
		loaderList->instanciateSpecial(it.first, it.second);
	}
}

void Piece::specialCalls(const PieceEvent &event)
{
	for (auto it : loaderList->specials) {

		switch (event) {
		case PIECE_EVENT_ON_EATEN:
			it->onEaten();
			break;
		case PIECE_EVENT_AT_TURN:
			it->atTurn();
			break;
		case PIECE_EVENT_AFTER_TURN:
			it->afterTurn();
			break;
		case PIECE_EVENT_AT_MOVE:
			it->atMove();
			break;
		case PIECE_EVENT_AFTER_MOVE:
			it->afterMove();
			break;
		case PIECE_EVENT_ON_INIT:
			it->onInit();
			break;
		default:
			break;
		}
	}
}
