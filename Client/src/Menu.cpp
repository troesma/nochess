#include "Menu.hpp"

#include <sys/types.h>
#include <dirent.h>
#include <regex>

Menu::Menu(Game *game)
	: game(game)
{
}

Menu::~Menu()
{
}

void Menu::startGame(const std::string &path)
{
	dialog->setVisible(false);
	game->launchGame(path);
}

void Menu::backgroundImage()
{
	auto image = g->images->create("./assets/menu/background_blurred.jpg");

	bg = new ox::Sprite();
	bg->setResAnim(image);
	bg->attachTo(group);
}

void Menu::dialogBox()
{
	auto image = g->images->create("./assets/menu/box.jpg");

	dialog = new ox::Box9Sprite;
	dialog->setPosition(200, 250);
	dialog->setResAnim(image);
	dialog->attachTo(ox::getStage());
	dialog->setVerticalMode(ox::Box9Sprite::STRETCHING);
	dialog->setHorizontalMode(ox::Box9Sprite::STRETCHING);
	dialog->setGuides(40, 40, 40, 40);

	dialog->setSize(1200, 600);

	dialog->attachTo(group);
}

void Menu::titleSprite()
{
	auto image = g->images->create("./assets/menu/ChessGame.png");

	title = new ox::Sprite;
	title->setPosition(512, 82);
	title->setResAnim(image);
	title->attachTo(ox::getStage());
	title->attachTo(group);
}

void Menu::setBoxContent()
{
	box = new ox::ClipRectActor;
	box->setPosition(23, 8);
	box->setSize(dialog->getWidth() - 46, dialog->getHeight() - 16);
	size_t i = 0;

	for (auto it : gameList) {
		items.push_back(new MenuItem(this, i, it));
		i++;
	}
	box->attachTo(dialog);
}

void Menu::start(std::shared_ptr<Graphical> g)
{
	this->g = g;
	readDirectory("Config");

	group = new ox::Actor();
	group->setSize(g->desc.w, g->desc.h);
	group->setPosition(0, 0);
	group->attachTo(ox::getStage());
	backgroundImage();
	dialogBox();
	titleSprite();
	setBoxContent();
}

void Menu::readDirectory(const std::string &name)
{
	DIR *dirp = opendir(name.c_str());
	struct dirent *dp;
	auto regx = std::regex(".+\\.json");

	while ((dp = readdir(dirp)) != NULL) {
		if (!std::regex_match(dp->d_name, regx))
			continue;
		gameList.push_back(dp->d_name);
	}
	closedir(dirp);
}
