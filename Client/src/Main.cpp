#include "Main.hpp"
#include "parameter.hpp"
#include "Game.hpp"

#include <iostream>
#include <unistd.h>

void usage(IParameter *parameter)
{
	std::cout << parameter->getFilename() << " [ CONFIG ]" << std::endl;
	std::cout << "\tCONFIG: config file of json format watch CONFIG directory" << std::endl;
}

int main(int ac, const char **av)
{
	std::shared_ptr<IParameter> parameter = std::make_shared<Parameter>(ac, av);

	parameter->option('h', "help", 0, "");
	parameter->option('c', "config", 1, "");
	try {
		parameter->parse();

		if (parameter->findParameter('h') != nullptr) {
			usage(parameter.get());
			return 0;
		}
		auto game = std::make_unique<Game>(parameter.get());
		game->init();
		game->start();
	} catch (const err::ExceptionThrower &e) {
		std::cerr << e.what() << std::endl;
		return 84;
	}
	return 0;
}
