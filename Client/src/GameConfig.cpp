#include "GameConfig.hpp"

#include <cstdio>

#include "Parser.hpp"
#include "Array.hpp"

GameConfig::GameConfig(GameLoader *loader, const std::string &config)
	: loader(loader)
	, config(config)
{
}

GameConfig::~GameConfig()
{
}

void GameConfig::getSquares(Json::Object &object)
{
	object.each<Json::Object>([&](const std::string &name, Json::Object &object) {
			  if (this->squaresSpecialsCfg[name] != nullptr)
				  throw err::excep(std::string("field: squares > ") + name + " is doubled");

			  auto cfg(std::make_shared<SquaresSpecialsConfig>());

			  object.child("board1")
				  ->as<std::string>([&cfg](std::string &board1) {
					  cfg->board1 = board1;
				  })
				  ->child("board2")
				  ->as<std::string>([&cfg](std::string &board2) {
					  cfg->board2 = board2;
				  })
				  ->child("specials")
				  ->as<Json::Object>([&cfg](Json::Object &object) {
					  object.each<Json::Object>([cfg](const std::string &name, Json::Object &object) {
							  // TODO : just change object to a reference
								cfg->params[name] = &object;
							})
						  ->parse();
				  })
				  ->parse();

			  squaresSpecialsCfg[name] = cfg;
		  })
		->parse();
}

void GameConfig::getPieces(Json::Object &object)
{
	auto lb = [&](const std::string &name, Json::Object &object) {
		if (squaresSpecialsCfg[name] != nullptr)
			throw err::excep("field: pieces > " + name + " is doubled");

		auto cfg(std::make_shared<PieceConfig>());

		object.child("black")
			->as<std::string>([&cfg](std::string &black) {
				cfg->black = black;
			})
			->child("white")
			->as<std::string>([&cfg](std::string &white) {
				cfg->white = white;
			})
			->child("specials")
			->as<Json::Object>([&cfg](Json::Object &object) {
					// TODO: This was changed from a Json::Value to a Json::Object
				object.each<Json::Object>([&cfg](std::string &name, Json::Object &value) {
						  //if (value.value == nullptr)
							  //std::cerr << "Error occured QQQQQQQQQQQQQQQQ" << std::endl;
						  //std::cout << name << std::endl;
						  // TODO No is
						  //value.is(Json::OBJECT);
						  //std::cout << "done" << std::endl;
						  // TODO change reference
						  // This get casted down
						  cfg->params[name] = &value;
					  })
					->parse();
			})
			->child("positions")
			->as<Json::Array>([&cfg](Json::Array &array) {
				array.each<Json::Array>([cfg](uint32_t &, Json::Array &array) {
						 Vector<int, 5> pos;
						 size_t size = 0;

						 array.each<int>([&pos, &size](uint32_t &, int &i) {
								  if (size > 5)
									  throw err::excep("Invalid field size in positions");
								  pos[size] = i;
								  size++;
							  })
							 ->parse();
						 if (size != 5)
							 throw err::excep("Invalid field size in positions");
						 cfg->movement.push_back(pos);
						 cfg->eating.push_back(pos);
					 })
					->parse();
			})
			->as<Json::Object>([&cfg](Json::Object &object) {
				object.child("movement")
					->as<Json::Array>([&cfg](Json::Array &array) {
						array.each<Json::Array>([cfg](uint32_t &, Json::Array &array) {
								 Vector<int, 5> pos;
								 size_t size = 0;

								 array.each<int>([&pos, &size](uint32_t &, int &i) {
										  if (size > 5)
											  throw err::excep("Invalid field size in positions");
										  pos[size] = i;
										  size++;
									  })
									 ->parse();
								 if (size != 5)
									 throw err::excep("Invalid field size in positions");
								 cfg->movement.push_back(pos);
							 })
							->parse();
					})
					->child("eating")
					->as<Json::Array>([&cfg](Json::Array &array) {
						array.each<Json::Array>([cfg](uint32_t &, Json::Array &array) {
								 Vector<int, 5> pos;
								 uint8_t size = 0;

								 array.each<int>([&pos, &size](uint32_t &, int &i) {
										  if (size > 5)
											  throw err::excep("Invalid field size in positions");
										  pos[size] = i;
										  size++;
									  })
									 ->parse();
								 if (size != 5)
									 throw err::excep("Invalid field size in positions");
								 cfg->eating.push_back(pos);
							 })
							->parse();
					})
					->parse();
			})
			->parse();
		piecesCfg[name] = cfg;
	};

	object.each<Json::Object>(lb)
		->each<Json::Parser>([&](const std::string &/*name*/, Json::Parser &/*parser*/) {
			// TODO: GET should give reference
			//Json::Object &object = parser.is(Json::OBJECT)->cast<Json::Object>().get();
			//lb(name, object);
		})
		->parse();
}

void GameConfig::getTeams(Json::Object &object)
{
	auto lb = [this](Json::Object &object) -> std::shared_ptr<TeamConfig> {
		Vector<int, 2> pos;
		std::string name;

		object.child("position")
			->as<Json::Array>([&pos](Json::Array &array) {
				uint8_t size = 0;

				array.each<int>([&size, &pos](uint32_t &, int &i) {
						 if (size > 2)
							 throw err::excep("Invalid Size.");
						 pos[size] = i;
						 size++;
					 })
					->parse();
				if (size != 2)
					throw err::excep("Invalid Size.");
			})
			->required()
			->child("name")
			->as<std::string>([&name](std::string &str) {
				name = str;
			})
			->required()
			->parse();

		if (teamsCfg[pos.array].get() != nullptr)
			throw err::excep("Invalid position already used.");

		if (piecesCfg[name].get() == nullptr)
			throw err::excep("Got an empty square.");

		auto cfg(std::make_shared<TeamConfig>());

		cfg->piece = piecesCfg[name];
		teamsCfg[pos.array] = cfg;
		return cfg;
	};

	object.child("white")
		->as<Json::Array>([&lb](Json::Array &array) {
			array.each<Json::Object>([&lb](uint32_t &, Json::Object &object) {
					 auto cfg = lb(object);

					 cfg->owner = WHITE;
				 })
				->parse();
		})
		->required()
		->child("black")
		->as<Json::Array>([&lb](Json::Array &array) {
			array.each<Json::Object>([&lb](uint32_t &, Json::Object &array) {
					 auto cfg = lb(array);

					 cfg->owner = BLACK;
				 })
				->parse();
		})
		->required()
		->parse();
}

void GameConfig::getMap(Json::Object &object)
{
	mapCfg = std::make_shared<MapConfig>();

	object.child("xy")
		->as<Json::Array>([this](Json::Array &array) {
			uint8_t size = 0;

			array.each<int &>([&size, this](uint32_t &, int i) {
					 if (size > 2)
						 throw err::excep("Invalid Size.");
					 mapCfg->xy[size] = i;
					 size++;
				 })
				->parse();
			std::cout << size << std::endl;
			if (size != 2)
				throw err::excep("Invalid Size.");
		})
		->required()
		->child("base")
		->as<std::string>([this](std::string &base) {
			mapCfg->base = base;
		})
		->required()
		->child("additionals")
		->as<Json::Object>([this](Json::Object &object) {
			(void)this;
			(void)object;
			//object->forEach([this](const std::string &name, Json::Array *array) {
			//auto cfg = squaresSpecialsCfg[name];
			//uint8_t size;
			//Vector<int, 2> pos;

			//array->forEach(std::function<void(int)>([&size, &pos](int i) {
			//if (size > 2)
			//throw err::excep("Invalid Size.");
			//pos[size] = i;
			//size++;
			//}))
			//->parse();
			//if (size != 2)
			//throw err::excep("Invalid Size.");
			//auto square = squaresCfg[pos.array];

			//if (square == nullptr) {
			//square = std::make_shared<SquaresConfig>();
			//squaresCfg[pos.array] = square;
			//}
			//square->specials.push_back(cfg);
			//square->specialsSize++;
			//})
			//->required()
			//->parse();
		})
		->parse();
}

void GameConfig::parse(const std::string &file)
{
	Json::Parser *parser = new Json::Parser();

	parser->file(file)
		->cast<Json::Object>()
		->child("squares")
		->as<Json::Object>([this](Json::Object &object) {
			this->getSquares(object);
		})
		->required()
		->child("pieces")
		->as<Json::Object>([this](Json::Object &object) {
			this->getPieces(object);
		})
		->child("map")
		->as<Json::Object>([this](Json::Object &object) {
			this->getMap(object);
		})
		->required()
		->child("teams")
		->as<Json::Object>([this](Json::Object &object) {
			this->getTeams(object);
		})
		->required()
		->parse();

	delete parser;
}

void GameConfig::checkIntegrity()
{
	try {
		this->parse(config);
	} catch (const Json::Exception &c) {
		throw err::excep(c.what());
	}
}
