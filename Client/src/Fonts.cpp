#include "Fonts.hpp"

Fonts::Fonts()
{
}

Fonts::~Fonts()
{
}

ox::ResFontBM *Fonts::create(const std::string &path)
{
	auto font = fonts[path];

	if (font == nullptr) {
		font = new ox::ResFontBM();
		if (font == nullptr)
			throw err::excep("Out of Memory.");
		font->init(path.c_str());
		font->load();
		fonts[path] = font;
	}
	return font;
}
