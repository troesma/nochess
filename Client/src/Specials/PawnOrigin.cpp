#include "Specials/PawnOrigin.hpp"
#include "Piece.hpp"

#include <algorithm>

PawnOrigin::PawnOrigin(Piece *piece, Json::Value *)
	: Specials(piece)
{
	//ConfigStandard config = this->configStandard(config);
}

PawnOrigin::~PawnOrigin()
{
}

Specials *PawnOrigin::create(Piece *piece, Json::Value *value)
{
	Specials *ptr = new PawnOrigin(piece, value);

	if (ptr == nullptr)
		throw err::excep("Out Of Memory.");
	return ptr;
}

void PawnOrigin::afterMove() {
	auto it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vec);

	if (it == piece->allMoves.end())
		return;
	piece->allMoves.erase(it);
	destroy();
}

void PawnOrigin::onInit()
{
	vec = std::vector<Vector<int16_t, 2>>({
		Vector<int16_t, 2>({0, 1}),
		Vector<int16_t, 2>({0, 2}),
	});
	piece->allMoves.push_back(vec);
	std::cout << "Piece Event On Init" << std::endl;
}
