#include "Specials/Rock.hpp"
#include "Piece.hpp"

#include <iostream>

Rock::Rock(Piece *piece, Json::Value *)
	: Specials(piece)
{
}

Rock::~Rock()
{
}

Specials *Rock::create(Piece *piece, Json::Value *value)
{
	Specials *ptr = new Rock(piece, value);

	if (ptr == nullptr)
		throw err::excep("Out Of Memory.");
	return ptr;
}

void Rock::afterMove() {
	auto it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecLeft);

	if (it == piece->allMoves.end())
		return;
	piece->allMoves.erase(it);
	it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecRight);

	if (it == piece->allMoves.end())
		return;
	piece->allMoves.erase(it);
	destroy();
}

void Rock::onInit()
{
	vecLeft = std::vector<Vector<int16_t, 2>>({
		Vector<int16_t, 2>({3, 0}),
	});
	vecRight = std::vector<Vector<int16_t, 2>>({
		Vector<int16_t, 2>({-2, 0}),
	});
	std::cout << piece->allMoves.size() << std::endl;
	piece->allMoves.push_back(vecLeft);
	piece->allMoves.push_back(vecRight);
	std::cout << "Piece Event On Init" << std::endl;
}
