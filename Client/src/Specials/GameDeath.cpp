#include "Specials/GameDeath.hpp"
#include "Piece.hpp"

#include <algorithm>

GameDeath::GameDeath(Piece *piece, Json::Value *)
	: Specials(piece)
{
}

GameDeath::~GameDeath()
{
}

Specials *GameDeath::create(Piece *piece, Json::Value *value)
{
	Specials *ptr = new GameDeath(piece, value);

	if (ptr == nullptr)
		throw err::excep("Out Of Memory.");
	return ptr;
}

void GameDeath::onEaten()
{
	if (piece->config->owner == WHITE)
		piece->loader->win(BLACK);
	if (piece->config->owner == BLACK)
		piece->loader->win(WHITE);
}
