#include "Specials/Castle.hpp"
#include "Piece.hpp"
#include "Object.hpp"
#include "Array.hpp"

#include <iostream>

Castle::Castle(Piece *piece, Json::Value *value)
	: Specials(piece)
{
	value->is(Json::OBJECT)
		->cast<Json::Object>()
		->child("conditions")
		->as([this](Json::Object *object) {
			object->child("positions")
				->as([this](Json::Array *array) {
					array->forEach([this](Json::Array *array) {
							 std::string pieceName;
							 Vector<int, 5> pos;

							 if (array->size() != 3)
								 throw Json::Exception("Invalid array size.");
							 int incr = 0;

							 array->at(0, [&pieceName](const std::string &_name) {
									  pieceName = _name;
								  })
								 ->forEach(std::function<void(int)>([&pos, &incr](int i) {
									 pos[incr] = i;
									 incr++;
								 }))
								 ->parse();
							 this->positions.push_back(std::pair<std::string, Vector<int, 5>>(pieceName, pos));
						 })
						->parse();
				})
				->required()
				->child("notmenaced")
				->as([this](Json::Array *array) {
					array->forEach([this](Json::Array *array) {
							 Vector<int, 2> tmp;

							 if (array->size() != 2)
								 throw Json::Exception("Invalid array size.");
							 int incr = 0;

							 array->forEach(std::function<void(int)>([&tmp, &incr](int i) {
									  tmp[incr] = i;
									  incr++;
								  }))
								 ->parse();
							 this->notmenaced.push_back(tmp);
						 })
						->parse();
				})
				->required()
				->child("nevermoved")
				->as([this](Json::Array *array) {
					array->forEach([this](Json::Array *array) {
							 std::string pieceName;
							 Vector<int, 5> pos;

							 if (array->size() != 3)
								 throw Json::Exception("Invalid array size.");
							 int incr = 0;

							 array->at(0, [&pieceName](const std::string &_name) {
									  pieceName = _name;
								  })
								 ->forEach(std::function<void(int)>([&pos, &incr](int i) {
									 pos[incr] = i;
									 incr++;
								 }))
								 ->parse();
							 this->nevermoved.push_back(std::pair<std::string, Vector<int, 5>>(pieceName, pos));
						 })
						->parse();
				})
				->required()
				->parse();
		})
		->required()
		->child("actions")
		->as([this](Json::Object *object) {
			object->child("move")
				->as([this](Json::Array *array) {
					array->at(0, [](Json::Array *array) {
							 if (array->size() != 2)
								 throw Json::Exception("Invalid array size.");
						 })
						->required()
						->at(1, [](Json::Array *array) {
							if (array->size() != 5)
								throw Json::Exception("Invalid array size.");
						})
						->required()
						->parse();
				})
				->required()
				->parse();
		});
}

Castle::~Castle()
{
}

Specials *Castle::create(Piece *piece, Json::Value *value)
{
	Specials *ptr = new Castle(piece, value);

	if (ptr == nullptr)
		throw err::excep("Out Of Memory.");
	return ptr;
}

void Castle::afterMove()
{
	//auto it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecLeft);

	//if (it == piece->allMoves.end())
	//return;
	//piece->allMoves.erase(it);
	//it = std::find(piece->allMoves.begin(), piece->allMoves.end(), vecRight);

	//if (it == piece->allMoves.end())
	//return;
	//piece->allMoves.erase(it);
	destroy();
}

void Castle::onInit()
{
	//vecLeft = std::vector<Vector<int16_t, 2>>({
	//Vector<int16_t, 2>(3, 0),
	//});
	//vecRight = std::vector<Vector<int16_t, 2>>({
	//Vector<int16_t, 2>(-2, 0),
	//});
	//std::cout << piece->allMoves.size() << std::endl;
	//piece->allMoves.push_back(vecLeft);
	//piece->allMoves.push_back(vecRight);
	//std::cout << "Piece Event On Init" << std::endl;
}
