#include "MenuItem.hpp"

MenuItem::MenuItem(Menu *menu, const uint32_t &index, const std::string &content)
	: content(content)
	, menu(menu)
	, group(new ox::Actor)
{
	group->setPosition(0, index * 100);
	group->setSize(menu->box->getWidth(), 100);
	hi = new ox::ColorRectSprite;
	hi->setPosition(0, 0);
	hi->setSize(group->getWidth(), group->getHeight());
	hi->setColor(ox::Color(112, 112, 112, 20));
	hi->attachTo(group);
	hi->setVisible(false);
	group->attachTo(menu->box);
	group->addEventListener(ox::TouchEvent::OVER, CLOSURE(this, &MenuItem::onOver));
	group->addEventListener(ox::TouchEvent::OUTX, CLOSURE(this, &MenuItem::onOver));
	group->addEventListener(ox::TouchEvent::CLICK, CLOSURE(this, &MenuItem::onClick));

	ox::ResFontBM *font = menu->game->g->fonts->create("./assets/fonts/font3.fnt");

	text = new ox::TextField;
	text->setText(content);
	text->setSize(menu->box->getWidth(), 100);
	text->setAlign(ox::TextStyle::VALIGN_MIDDLE, ox::TextStyle::HALIGN_MIDDLE);
	text->setFont(font);
	text->setColor(ox::Color::White);
	text->attachTo(group);
}

MenuItem::~MenuItem()
{
}

void MenuItem::onClick(ox::Event *event)
{
	auto touchEvent = ox::safeCast<ox::TouchEvent *>(event);

	if (touchEvent->mouseButton != ox::MouseButton_Left)
		return;
	menu->startGame("./Config/" + content);
}

void MenuItem::onOver(ox::Event *event)
{
	if (event->type == ox::TouchEvent::OVER) {
		hi->setVisible(true);
	} else {
		hi->setVisible(false);
	}
}

void MenuItem::setContent(const std::string &_content)
{
	content = _content;
}
