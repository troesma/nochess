#include "Game.hpp"

#include <iostream>

Game::Game(IParameter *parameter)
	: parameter(parameter)
{
}

void Game::launchGame(const std::string &path)
{
	loader = std::make_shared<GameLoader>(this, path);
	loader->start(g);
}

void Game::init()
{
	g = std::make_shared<Graphical>();
	g->init();
	ArgsData *data = parameter->findParameter('c');
	if (data != nullptr) {
		launchGame(data->args.at(0));
	} else {
		menu = std::make_shared<Menu>(this);
		menu->start(g);
	}
}

void Game::update()
{
	if (loader != nullptr && loader->gameEnded == true) {
		loader = nullptr;
		menu->dialog->setVisible(true);
	}
}

void Game::start()
{
	while (g->update()) {
		this->update();
	}
	g->destroy();
}

Game::~Game()
{
}
