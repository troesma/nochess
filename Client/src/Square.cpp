#include "Square.hpp"

Square::Square(GameLoader *loader, Vector<int16_t, 2> xy, const SquaresConfig *config)
	: xy(xy)
	, piece(nullptr)
	, config(config)
	, loader(loader)
{
}

Square::~Square()
{
	//if (tile)
		//delete tile;
	//if (hiAlly)
		//delete hiAlly;
	//if (hiEnemy)
		//delete hiEnemy;
	//if (group) {
	//group->removeAllEventListeners();
		//delete group;
}

void Square::init()
{
	Vector<float, 4> area = loader->board->area;
	Vector<float, 2> size({area[0] / loader->xy[0], area[1] / loader->xy[1]});
	Vector<float, 2> pos = size * Vector<float, 2>({(float)xy[0], (float)xy[1]});

	SquaresSpecialsConfig *base = loader->config->squaresSpecialsCfg[loader->config->mapCfg->base].get();
	ox::SingleResAnim *image;
	if ((xy[0] + xy[1]) % 2) {
		image = loader->g->images->create(base->board1);
	} else {
		image = loader->g->images->create(base->board2);
	}

	Vector<float, 2> scale = size / Vector<float, 2>({image->getWidth(), image->getHeight()});

	group = new ox::Actor();
	tile = new ox::Sprite();
	group->setSize(size[0], size[1]);
	group->setPosition(pos[0], pos[1]);
	group->addEventListener(ox::TouchEvent::CLICK, CLOSURE(this, &Square::buttonEvent));
	group->addEventListener(ox::TouchEvent::TOUCH_DOWN, CLOSURE(this, &Square::slideEventDown));
	tile->setResAnim(image);
	tile->setScale(scale[0], scale[1]);
	tile->attachTo(group);
	group->attachTo(loader->board->zone);

	hiAlly = new ox::ColorRectSprite;
	hiAlly->setPosition(0, 0);
	hiAlly->setSize(group->getWidth(), group->getHeight());
	hiAlly->setColor(ox::Color(0, 255, 45, 51));
	hiAlly->attachTo(group);
	hiAlly->setVisible(false);

	hiEnemy = new ox::ColorRectSprite;
	hiEnemy->setPosition(0, 0);
	hiEnemy->setSize(group->getWidth(), group->getHeight());
	hiEnemy->setColor(ox::Color(255, 255, 51, 81));
	hiEnemy->attachTo(group);
	hiEnemy->setVisible(false);
}

void Square::highlight(bool state, bool color)
{
	if (!state) {
		hiAlly->setVisible(state);
		hiEnemy->setVisible(state);
	} else if (!color)
		hiAlly->setVisible(state);
	else
		hiEnemy->setVisible(state);
}

void Square::playTurn()
{
	loader->board->endTurn();
	loader->turn++;
	loader->board->active = nullptr;
	loader->board->beginTurn();
}

void Square::buttonEvent(ox::Event *event)
{
	auto touchEvent = ox::safeCast<ox::TouchEvent *>(event);

	if (touchEvent->mouseButton != ox::MouseButton_Left)
		return;
	buttonClick();
}

void Square::slideEventDown(ox::Event *)
{
	if (piece == nullptr)
		return;
	if (loader->board->active != nullptr) {
		loader->board->active->unselect();
		if (loader->board->active->config->owner != this->piece->config->owner)
			return;
	}
	loader->board->active = this->piece;
	loader->board->active->texture->setPriority(10);
	this->piece->select();
	loader->board->active->resetScale(loader->board->scale);
	ox::Vector2 pos = piece->texture->local2stage(0, 0);
	piece->texture->attachTo(loader->board->zone);
	pos = loader->board->zone->stage2local(pos);
	piece->texture->setPosition(pos);
	loader->board->zone->addEventListener(ox::TouchEvent::MOVE, CLOSURE(piece, &Piece::move));
}

void Square::buttonClick()
{
	if (loader->board->active != nullptr) {
		loader->board->active->moved = false;
		if (loader->board->active->moveTo(this)) {
			playTurn();
		} else if (this->piece != nullptr) {
			loader->board->active = this->piece;
			this->piece->select();
		} else {
			loader->board->active = nullptr;
		}
	} else if (this->piece != nullptr) {
		loader->board->active = this->piece;
		this->piece->select();
	}
}


void Square::addPiece(Piece *newPiece)
{
	if (this->piece != nullptr)
		this->piece->getEatenBy(newPiece);
	this->piece = newPiece;

	piece->texture->detach();
	piece->texture->attachTo(group);
}

void Square::loadSpecial()
{
	//loaderList = std::make_shared<SpecialsLoaderSquare>(this);
	//for (auto it : config->piece->params)
	//loaderList->instanciateSpecial(it.first, it.second);
}

void Square::specialCalls(const SquareEvent &event)
{
	//for (auto it : loaderList->specials) {

	//switch (event) {
	//case SQUARE_EVENT_ON_EATEN:
	//it->onEaten();
	//break;
	//case SQUARE_EVENT_AT_TURN:
	//it->atTurn();
	//break;
	//case SQUARE_EVENT_AFTER_TURN:
	//it->afterTurn();
	//break;
	//case SQUARE_EVENT_AT_MOVE:
	//it->atMove();
	//break;
	//case SQUARE_EVENT_AFTER_MOVE:
	//it->afterMove();
	//break;
	//case SQUARE_EVENT_ON_INIT:
	//it->onInit();
	//break;
	//default:
	//break;
	//}
	//}
}
