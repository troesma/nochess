#include "Board.hpp"

Board::Board(GameLoader *loader)
	: active(nullptr)
	, loader(loader)
	, xy(loader->xy)
	, area({ 800, 800, 400, 70 })
{
}

void Board::init()
{
	scale = ox::Vector2(area[0] / loader->board->xy[0], area[1] / loader->board->xy[1]);
	zone = new ox::ClipRectActor;
	zone->setSize(area[0], area[1]);
	zone->setPosition(area[2], area[3]);
	zone->attachTo(ox::getStage());
	zone->addEventListener(ox::TouchEvent::TOUCH_UP, CLOSURE(this, &Board::slideEventUp));
	for (int16_t x = 0; x < (uint16_t)loader->xy[0]; x++) {
		std::vector<std::shared_ptr<Square>> squares;

		for (int16_t y = 0; y < (int16_t)loader->xy[1]; y++) {
			std::shared_ptr<SquaresConfig> cfg = loader->config->squaresCfg[std::array<int, 2>({ x, y })];
			auto square = std::make_shared<Square>(loader, Vector<int16_t, 2>({x, y}), cfg.get());

			square->init();
			squares.push_back(square);
		}
		board.push_back(squares);
	}
}

Square *Board::findSquarePx(int x, int y)
{
	x /= (area[0] / xy[0]);
	y /= (area[1] / xy[1]);
	if (x < 0 || x >= xy[0] || y < 0 || y >= xy[1])
		return nullptr;
	return board.at(x).at(y).get();
}

void Board::slideEventUp(ox::Event *event)
{
	if (loader->board->active != nullptr) {
		loader->board->active->texture->setPriority(0);
		loader->board->zone->removeEventListener(ox::TouchEvent::MOVE, CLOSURE(loader->board->active, &Piece::move));
		loader->board->active->texture->setPosition(0, 0);
		if (loader->board->active->moved == false) {
			active->texture->attachTo(active->square->group);
			return;
		}
		loader->board->active->moved = false;

		auto te = ox::safeCast<ox::TouchEvent *>(event);

		ox::Vector2 local = te->localPosition;
		Square *touched = findSquarePx(local.x, local.y);
		if (touched == nullptr) {
			loader->board->active->unselect();
			loader->board->active->texture->detach();
			loader->board->active->texture->attachTo(loader->board->active->square->group);
			loader->board->active = nullptr;
			return;
		}
		if (loader->board->active->moveTo(touched))
			touched->playTurn();
		loader->board->active = nullptr;
	}
}

void Board::beginTurn()
{
	for (auto line : board) {
		for (auto square : line) {
			if (square->piece == nullptr)
				continue;
			square->piece->specialCalls(PIECE_EVENT_AT_TURN);
		}
	}
}

void Board::endTurn()
{
	for (auto line : board) {
		for (auto square : line) {
			if (square->piece == nullptr)
				continue;
			square->piece->specialCalls(PIECE_EVENT_AFTER_TURN);
		}
	}
}

Board::~Board()
{
}

Square *Board::at(uint32_t x, uint32_t y)
{
	if (x <= 0 || x > xy[0] || y <= 0 || y > xy[1])
		throw err::excep("Invalid at position : " + std::to_string(x) + ", " + std::to_string(y));
	return board.at(x - 1).at(y - 1).get();
}

Square *Board::at(Vector<int, 2> pos)
{
	if (pos[0] <= 0 || pos[0] > (int)xy[0] || pos[1] <= 0 || pos[1] > (int)xy[1])
		throw err::excep("Invalid at position : " + pos.str());
	return board.at(pos[0] - 1).at(pos[1] - 1).get();
}
