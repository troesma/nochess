#include "Specials.hpp"
#include "Piece.hpp"

Specials::Specials(Piece *piece)
	: piece(piece)
{
}

Specials::~Specials()
{
}

void Specials::destroy()
{
	auto it = std::find(piece->loaderList->specials.begin(), piece->loaderList->specials.end(), this);

	if (it == piece->loaderList->specials.end())
		throw err::excep("Couldn't find specials.");
	piece->loaderList->specials.erase(it);
	delete this;
}
