#include "Images.hpp"

Images::Images()
{
}

Images::~Images()
{
}

ox::SingleResAnim *Images::create(const std::string &path)
{
	auto image = images[path];

	if (image == nullptr) {
		image = new ox::SingleResAnim();
		if (image == nullptr)
			throw err::excep("Out of Memory.");
		image->init(path);
		images[path] = image;
	}
	return image;
}
