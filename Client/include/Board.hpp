#pragma once

class Board;

#include "Exception.hpp"
#include "GameLoader.hpp"
#include "Square.hpp"
#include "Piece.hpp"

#include <vector>

//! What happens if I do that
/*!
 * The Board class is used to access each Square of the Board,
 * It also handle Drag and Drop, the Timer and the player turn.
 */
class Board
{
	public:
	Board(GameLoader *loader);
	virtual ~Board();
	Square *at(uint32_t x, uint32_t y);
	Square *at(Vector<int, 2> pos);
	void init();
	void setActive();
	void beginTurn();
	void endTurn();
	Square *findSquarePx(int x, int y);
	void slideEventUp(ox::Event *);

	public:
	Piece *active;
	GameLoader *loader;
	Vector<int64_t, 2> xy;
	Vector<float, 4> area;
	ox::Vector2 scale;
	std::vector<std::vector<std::shared_ptr<Square>>> board;
	ox::ClipRectActor *zone;
};
