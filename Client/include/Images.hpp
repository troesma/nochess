#pragma once

class Images;

#include <memory>

#include "GameLoader.hpp"

#include <unordered_map>
#include <string>

class Images
{
	public:
	Images();
	virtual ~Images();
	ox::SingleResAnim *create(const std::string &path);

	private:
	std::unordered_map<std::string, ox::SingleResAnim*> images;
};
