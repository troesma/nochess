#pragma once

class MenuItem;

#include "Graphical.hpp"
#include "Menu.hpp"

#include <stdint.h>

class MenuItem
{
	public:
	MenuItem(Menu *menu, const uint32_t &, const std::string &);
	void setContent(const std::string &);
	virtual ~MenuItem();
	void onOver(ox::Event *);
	void onClick(ox::Event *);

	std::string content;
	Menu *menu;
	ox::ColorRectSprite *hi;
	ox::spActor group;
	ox::spTextField text;
};
