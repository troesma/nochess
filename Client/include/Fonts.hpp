#pragma once

class Fonts;

#include <memory>

#include "GameLoader.hpp"

#include <unordered_map>
#include <string>

class Fonts
{
	public:
	Fonts();
	virtual ~Fonts();
	ox::ResFontBM *create(const std::string &path);

	private:
	std::unordered_map<std::string, ox::ResFontBM *> fonts;
};
