#pragma once

class Graphical;

#include "Images.hpp"
#include "Fonts.hpp"
#include "GameLoader.hpp"

class Graphical
{
	public:
	Graphical();
	virtual ~Graphical();
	void init();
	bool update();
	void predestroy();
	void destroy();

	public:
	ox::core::init_desc desc;
	std::shared_ptr<Images> images;
	std::shared_ptr<Fonts> fonts;
};
