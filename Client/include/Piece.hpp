#pragma once

class Piece;

#include "GameConfig.hpp"
#include "Square.hpp"
#include "GameLoader.hpp"
#include "SpecialsLoaderPiece.hpp"
#include "Vector.hpp"
#include "Oxygine.hpp"

#include <stdint.h>
#include <vector>
#include <memory>

enum PieceEvent {
	PIECE_EVENT_NONE,
	PIECE_EVENT_AT_TURN,
	PIECE_EVENT_AFTER_TURN,
	PIECE_EVENT_AT_MOVE,
	PIECE_EVENT_AFTER_MOVE,
	PIECE_EVENT_ON_INIT,
	PIECE_EVENT_ON_EATEN,
};

enum PieceDirection {
	PIECE_STILL,
	PIECE_TOP,
	PIECE_RIGHT,
	PIECE_BOTTOM,
	PIECE_LEFT,
};

class Piece
{
	public:
	Piece(GameLoader *loader, Square *square, const TeamConfig *config);
	virtual ~Piece();
	bool moveTo(Square *newsquare);
	bool possibleMove(Square *newsquare);
	std::vector<Vector<int16_t, 2>> listMoves();
	void start();
	void select();
	void unselect();
	void move(ox::Event *event);

	void loadSpecial();
	void resetScale(ox::Vector2 tsize);
	void setupPiece(std::shared_ptr<PieceConfig> config);
	void generateConfig(std::shared_ptr<PieceConfig> config);
	void getEatenBy(Piece *);

	public:
	void specialCalls(const PieceEvent &event);

	public:
	Square *square;
	std::vector<std::vector<Vector<int16_t, 2>>> allMoves;
	std::vector<std::vector<Vector<int16_t, 2>>> allAttacks;

	std::vector<Vector<int16_t, 2>> moves;
	GameLoader *loader;
	const TeamConfig *config;
	std::shared_ptr<SpecialsLoaderPiece> loaderList;
	ox::SingleResAnim *image;
	ox::spSprite texture;
	bool moved;
};
