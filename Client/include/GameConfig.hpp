#pragma once

class GameConfig;
struct PieceConfig;
struct MapConfig;
struct TeamConfig;
struct SquaresConfig;
struct SquaresSpecialsConfig;

#include "Vector.hpp"
#include "Specials.hpp"
#include "Object.hpp"

#include <rapidjson/document.h>
#include <rapidjson/filereadstream.h>
#include <memory>
#include <unordered_map>
#include <map>
#include <vector>

enum Team {
	NONE,
	BLACK,
	WHITE,
};

struct SquaresSpecialsConfig {
	std::map<std::string, Json::Object *> params;
	std::string board1 = "./assets/default/board1.png";
	std::string board2 = "./assets/default/board2.png";
};

struct SquaresConfig {
	std::vector<std::shared_ptr<SquaresSpecialsConfig>> specials;
	size_t specialsSize;
};

struct PieceConfig {
	std::map<std::string, Json::Value *> params;
	std::string white = "./assets/default/piece_white.png";
	std::string black = "./assets/default/piece_black.png";
	std::vector<Vector<int, 5>> movement;
	std::vector<Vector<int, 5>> eating;
};

struct TeamConfig {
	std::shared_ptr<PieceConfig> piece;
	Team owner;
};

struct MapConfig {
	Vector<int64_t, 2> xy = std::array<int64_t, 2>({ 8, 8 });
	std::string base = "default";
};

#include "GameLoader.hpp"

class GameConfig
{
	public:
	GameConfig(GameLoader *loader, const std::string &config);
	virtual ~GameConfig();
	void parse(const std::string &);
	void checkIntegrity();
	void getSquares(Json::Object &object);
	void getPieces(Json::Object &object);
	void getTeams(Json::Object &object);
	void getMap(Json::Object &object);


	private:
	GameLoader *loader;

	public:
	std::string config;
	std::map<const std::string, std::shared_ptr<SquaresSpecialsConfig>> squaresSpecialsCfg;
	std::map<const std::string, std::shared_ptr<PieceConfig>> piecesCfg;
	std::map<std::array<int, 2>, std::shared_ptr<SquaresConfig>> squaresCfg;
	std::map<std::array<int, 2>, std::shared_ptr<TeamConfig>> teamsCfg;
	std::shared_ptr<MapConfig> mapCfg;
};
