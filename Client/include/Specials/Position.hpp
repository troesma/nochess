#pragma once

class Position;

#include "Specials.hpp"
#include "Vector.hpp"
#include "Array.hpp"

#include <vector>

class Position : virtual Specials
{
	public:
	Position(Piece *, Json::Value *);
	virtual ~Position();
	static Specials *create(Piece *piece, Json::Value *);

	private:
	Json::Array *params;
};
