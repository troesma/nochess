#pragma once

class Specials;
class Piece;
class Square;

#include "Exception.hpp"

#include "Value.hpp"

#include <rapidjson/document.h>
#include <memory>
#include <iostream>

class Specials
{
	public:
	using Initializer = Specials *(*)(Piece *piece, Json::Value *);

	Specials(Piece *);
	virtual ~Specials();

	virtual void atTurn() {};
	virtual void afterTurn() {};
	virtual void atMove() {};
	virtual void afterMove() {};
	virtual void onEaten() {};
	virtual void onInit() {};

	virtual void destroy();

	struct ConfigStandard {
	};

	public:
	std::string name;

	protected:
	Piece *piece;
};
