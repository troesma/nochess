#pragma once

class GameLoader;

#include "Oxygine.hpp"

#include "GameConfig.hpp"
#include "Game.hpp"
#include "Board.hpp"
#include "Square.hpp"
#include "Piece.hpp"
#include "Exception.hpp"
#include "Graphical.hpp"

#include <memory>
#include <map>

class GameLoader
{
	public:
	GameLoader(Game *game, std::string configFile);
	void start(std::shared_ptr<Graphical>);
	virtual ~GameLoader();
	void win(const Team &);
	std::map<const std::string, Specials::Initializer> &libLoader();

	public:
	Game *game;
	GameConfig *config;
	Board *board;
	std::vector<Piece *> pieces;
	Vector<int64_t, 2> xy;
	std::shared_ptr<Graphical> g;
	bool gameEnded = true;
	int turn;
	std::map<const std::string, Specials::Initializer> specialsInitializer;
	std::vector<void *> handles;
};
